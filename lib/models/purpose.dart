import 'package:cure_component/models/activity.dart';

class Purpose {
  final int id;
  final String name;
  final List<String> data;
  final bool isSelectable;
  final bool isSelected;
  final List<int> parentIds;
  final List<int> childIds;
  final List<Activity> activities;

  Purpose({
    this.id,
    this.name,
    this.data,
    this.isSelected,
    this.isSelectable,
    this.parentIds,
    this.childIds,
    this.activities,
  });

  Purpose copyWith({
    id = Symbol.empty,
    name = Symbol.empty,
    data = Symbol.empty,
    isSelected = Symbol.empty,
    isSelectable = Symbol.empty,
    parentIds = Symbol.empty,
    activities = Symbol.empty,
    childIds = Symbol.empty,
  }) {
    return Purpose(
      id: id != Symbol.empty ? id : this.id,
      name: name != Symbol.empty ? name : this.name,
      data: data != Symbol.empty ? data : this.data,
      isSelected: isSelected != Symbol.empty ? isSelected : this.isSelected,
      isSelectable:
          isSelectable != Symbol.empty ? isSelectable : this.isSelectable,
      parentIds: parentIds != Symbol.empty ? parentIds : this.parentIds,
      activities: activities != Symbol.empty ? activities : this.activities,
      childIds: childIds != Symbol.empty ? childIds : this.childIds,
    );
  }

  List<String> getData() {
    final result = Set<String>();
    activities.forEach((element) {
      element.processorActivity.forEach((key, value) {
        result.addAll(value);
      });
    });
    return result.toList();
  }

  static fromMap(Map<String, dynamic> data) {
    final activities = List<Activity>();
    data['activities'].map((x) => x).forEach((x) {
      activities.addAll(Activity.fromMap(x));
    });

    return Purpose(
      id: data['id'],
      name: data['name'],
      isSelected: false,
      isSelectable: false,
      data: data['data'].map<String>((x) => x.toString()).toList(),
      parentIds: data['parentIds'].map<int>((x) => x as int).toList(),
      childIds: data['childIds'].map<int>((x) => x as int).toList(),
      activities: activities,
    );
  }
}

import 'model.dart';

class ShopItem extends Model {
  static String table = 'shop_item';

  final int id;
  final int price;
  final String name;
  final String address;
  final String imageUri;

  ShopItem({
    this.id,
    this.price,
    this.name,
    this.address,
    this.imageUri,
  });

  ShopItem copyWith({
    id = Symbol.empty,
    price = Symbol.empty,
    name = Symbol.empty,
    address = Symbol.empty,
    imageUri = Symbol.empty,
  }) {
    return ShopItem(
      id: id != Symbol.empty ? id : this.id,
      price: price != Symbol.empty ? price : this.price,
      name: name != Symbol.empty ? name : this.name,
      address: address != Symbol.empty ? address : this.address,
      imageUri: imageUri != Symbol.empty ? imageUri : this.imageUri,
    );
  }

  static fromMap(Map<String, dynamic> data) => ShopItem(
        id: data['id'],
        price: data['price'],
        name: data['name'],
        address: data['address'],
        imageUri: data['imageUri'],
      );

  Map<String, dynamic> toMap() => {
        "id": this.id,
        "price": this.price,
        "name": this.name,
        "address": this.address,
        "imageUri": this.imageUri,
      };
}

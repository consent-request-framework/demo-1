import 'dart:io';

import 'package:demo1/models/model.dart';
import 'package:google_place/google_place.dart';

class UserReview extends Model {
  static String table = 'review';

  int id;
  int score;
  String placeId;
  String firstName;
  String lastName;
  String reviewText;
  String imageUrl;
  File image;
  DateTime timestamp;

  UserReview({
    this.id,
    this.score,
    this.placeId,
    this.firstName,
    this.lastName,
    this.reviewText,
    this.imageUrl,
    this.image,
    this.timestamp,
  });

  static fromMap(Map<String, dynamic> data) => UserReview(
        id: data['id'],
        score: data['score'] ?? 0,
        firstName: data['firstName'].toString(),
        lastName: data['lastName'].toString(),
        placeId: data['placeId'].toString(),
        image: File(data['imageUri'] ?? ''),
        reviewText: data['reviewText']?.toString(),
        timestamp: DateTime.fromMillisecondsSinceEpoch(data['timestamp']),
      );

  Map<String, dynamic> toMap() => {
        "id": this.id,
        "score": this.score ?? 0,
        "firstName": this.firstName,
        "lastName": this.lastName,
        "placeId": this.placeId,
        "imageUri": this.image?.path ?? '',
        "reviewText": this.reviewText,
        "timestamp": this.timestamp.millisecondsSinceEpoch,
      };

  static fromPlaceReview(Review placeReview) => UserReview(
        id: 0,
        score: placeReview.rating,
        firstName: placeReview.authorName,
        lastName: '',
        placeId: '',
        image: File(''),
        imageUrl: placeReview.profilePhotoUrl,
        reviewText: placeReview.text,
        timestamp: DateTime.now(),
      );

  String getAuthorName() {
    return '$firstName $lastName';
  }
}

import 'model.dart';

class UsagePurpose extends Model {
  static String table = 'usage_purpose';

  final int id;
  final int purposeId;
  final DateTime timestamp;

  UsagePurpose({
    this.id,
    this.purposeId,
    this.timestamp,
  });

  UsagePurpose copyWith({
    id = Symbol.empty,
    purposeId = Symbol.empty,
    timestamp = Symbol.empty,
  }) {
    return UsagePurpose(
      id: id != Symbol.empty ? id : this.id,
      purposeId: purposeId != Symbol.empty ? purposeId : this.purposeId,
      timestamp: timestamp != Symbol.empty ? timestamp : this.timestamp,
    );
  }

  static fromMap(Map<String, dynamic> data) => UsagePurpose(
        id: data['id'],
        purposeId: data['purposeId'],
        timestamp: DateTime.fromMillisecondsSinceEpoch(data['timestamp']),
      );

  Map<String, dynamic> toMap() => {
        "id": this.id,
        "purposeId": this.purposeId,
        "timestamp": this.timestamp.millisecondsSinceEpoch,
      };
}

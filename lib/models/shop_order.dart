import 'package:demo1/enums/shop_order_status_enum.dart';
import 'package:demo1/models/address.dart';

import 'model.dart';

class ShopOrder extends Model {
  static String table = 'shop_order';

  final int id;
  final int shopItemPrice;
  final String shopItemName;
  final String shopItemAddress;
  final String shopItemImageUri;
  final String firstName;
  final String lastName;
  final Address address;
  final DateTime timestamp;
  final ShopOrderStatusEnum status;

  ShopOrder({
    this.id,
    this.shopItemPrice,
    this.shopItemName,
    this.shopItemAddress,
    this.shopItemImageUri,
    this.firstName,
    this.lastName,
    this.address,
    this.timestamp,
    this.status,
  });

  ShopOrder copyWith({
    id = Symbol.empty,
    shopItemPrice = Symbol.empty,
    shopItemName = Symbol.empty,
    shopItemAddress = Symbol.empty,
    shopItemImageUri = Symbol.empty,
    firstName = Symbol.empty,
    lastName = Symbol.empty,
    address = Symbol.empty,
    timestamp = Symbol.empty,
    status = Symbol.empty,
  }) {
    return ShopOrder(
      id: id != Symbol.empty ? id : this.id,
      shopItemPrice:
          shopItemPrice != Symbol.empty ? shopItemPrice : this.shopItemPrice,
      shopItemName:
          shopItemName != Symbol.empty ? shopItemName : this.shopItemName,
      firstName: firstName != Symbol.empty ? firstName : this.firstName,
      shopItemAddress: shopItemAddress != Symbol.empty
          ? shopItemAddress
          : this.shopItemAddress,
      shopItemImageUri: shopItemImageUri != Symbol.empty
          ? shopItemImageUri
          : this.shopItemImageUri,
      lastName: lastName != Symbol.empty ? lastName : this.lastName,
      address: address != Symbol.empty ? address : this.address,
      timestamp: timestamp != Symbol.empty ? timestamp : this.timestamp,
      status: status != Symbol.empty ? status : this.status,
    );
  }

  static fromMap(Map<String, dynamic> data) => ShopOrder(
        id: data['id'],
        shopItemPrice: data['shopItemPrice'],
        shopItemName: data['shopItemName'],
        shopItemAddress: data['shopItemAddress'],
        shopItemImageUri: data['shopItemImageUri'],
        firstName: data['firstName'],
        lastName: data['lastName'],
        address: Address(
          street: data['street'],
          zipCode: data['zipCode'],
          city: data['city'],
        ),
        timestamp: DateTime.fromMillisecondsSinceEpoch(data['timestamp']),
        status: getStatus(data['status']),
      );

  Map<String, dynamic> toMap() => {
        "id": this.id,
        "shopItemPrice": this.shopItemPrice,
        "shopItemName": this.shopItemName,
        "shopItemAddress": this.shopItemAddress,
        "shopItemImageUri": this.shopItemImageUri,
        "firstName": this.firstName,
        "lastName": this.lastName,
        "street": this.address.street,
        "zipCode": this.address.zipCode,
        "city": this.address.city,
        "timestamp": this.timestamp.millisecondsSinceEpoch,
        "status": this.status?.toString(),
      };

  static ShopOrderStatusEnum getStatus(String value) {
    if (value == null) {
      return null;
    }
    var values = ShopOrderStatusEnum.values;
    return values.firstWhere((e) => e.toString() == value);
  }
}

import 'dart:io';

import 'package:demo1/enums/filter_group_enum.dart';
import 'package:demo1/models/model.dart';

import 'address.dart';

class Profile extends Model {
  static String table = 'profile';

  final int id;
  final String uuid;
  final File avatar;
  final String firstName;
  final String lastName;
  final int age;
  final int selectedTemplateId;
  final bool isConsentConfirmed;
  final List<int> selectedPurposeIds;
  final Set<FilterEnum> selectedMapFilters;
  final Address address;
  final DateTime timestamp;
  final String templates;

  Profile({
    this.id,
    this.uuid,
    this.avatar,
    this.firstName,
    this.lastName,
    this.age,
    this.selectedTemplateId,
    this.selectedPurposeIds,
    this.selectedMapFilters,
    this.address,
    this.timestamp,
    this.isConsentConfirmed,
    this.templates,
  });

  bool get hasConsent {
    return this.selectedTemplateId != null;
  }

  Profile copyWith({
    id = Symbol.empty,
    avatar = Symbol.empty,
    uuid = Symbol.empty,
    firstName = Symbol.empty,
    lastName = Symbol.empty,
    age = Symbol.empty,
    selectedTemplateId = Symbol.empty,
    selectedPurposeIds = Symbol.empty,
    selectedMapFilters = Symbol.empty,
    address = Symbol.empty,
    timestamp = Symbol.empty,
    isConsentConfirmed = Symbol.empty,
    templates = Symbol.empty,
  }) {
    return Profile(
      id: id != Symbol.empty ? id : this.id,
      avatar: avatar != Symbol.empty ? avatar : this.avatar,
      uuid: uuid != Symbol.empty ? uuid : this.uuid,
      firstName: firstName != Symbol.empty ? firstName : this.firstName,
      lastName: lastName != Symbol.empty ? lastName : this.lastName,
      age: age != Symbol.empty ? age : this.age,
      selectedTemplateId: selectedTemplateId != Symbol.empty
          ? selectedTemplateId
          : this.selectedTemplateId,
      selectedPurposeIds: selectedPurposeIds != Symbol.empty
          ? selectedPurposeIds
          : this.selectedPurposeIds,
      selectedMapFilters: selectedMapFilters != Symbol.empty
          ? selectedMapFilters
          : this.selectedMapFilters,
      address: address != Symbol.empty ? address : this.address,
      timestamp: timestamp != Symbol.empty ? timestamp : this.timestamp,
      templates: templates != Symbol.empty ? templates : this.templates,
      isConsentConfirmed: isConsentConfirmed != Symbol.empty
          ? isConsentConfirmed
          : this.isConsentConfirmed,
    );
  }

  static fromMap(Map<String, dynamic> data) => Profile(
        id: data['id'],
        uuid: data['uuid'],
        templates: data['templates']?.toString(),
        avatar: data['avatarUri'] != null ? File(data['avatarUri']) : null,
        firstName: data['firstName']?.toString(),
        lastName: data['lastName']?.toString(),
        age: data['age'] ?? 0,
        selectedTemplateId: data['selectedTemplateId'],
        isConsentConfirmed: data['isConsentConfirmed'] == 1,
        selectedPurposeIds: _parseIds(data['selectedPurposeIds']),
        selectedMapFilters: _parseFilters(data['selectedMapFilters']),
        address: Address(
          street: data['street']?.toString(),
          zipCode: data['zipCode'],
          city: data['city']?.toString(),
        ),
        timestamp: data['timestamp'] != null
            ? DateTime.fromMillisecondsSinceEpoch(data['timestamp'])
            : null,
      );

  Map<String, dynamic> toMap() => {
        "id": this.id,
        "uuid": this.uuid,
        "avatarUri": this.avatar?.path,
        "firstName": this.firstName,
        "templates": this.templates,
        "lastName": this.lastName,
        "age": this.age ?? 0,
        "isConsentConfirmed": this.isConsentConfirmed ? 1 : 0,
        "selectedTemplateId": this.selectedTemplateId,
        "selectedPurposeIds": _serializeIds(this.selectedPurposeIds),
        "selectedMapFilters": _serializeFilters(this.selectedMapFilters),
        "street": this.address?.street,
        "zipCode": this.address?.zipCode,
        "city": this.address?.city,
        "timestamp": this.timestamp?.millisecondsSinceEpoch,
      };

  bool isTemplateSelected(int templateId) {
    return selectedTemplateId == templateId;
  }

  bool isPurposeSelected(int purposeId) {
    if (selectedPurposeIds == null) {
      return false;
    }
    return selectedPurposeIds.contains(purposeId);
  }

  bool isFilterSelected(FilterEnum filter) {
    return selectedMapFilters.contains(filter);
  }

  static List<int> _parseIds(value) {
    String converted = value?.toString();
    if (converted == null || converted.isEmpty) {
      return [];
    }

    return converted.split(',').map<int>((x) => int.parse(x)).toList();
  }

  static Set<FilterEnum> _parseFilters(String value) {
    String converted = value?.toString();
    if (converted == null || converted.isEmpty) {
      return Set();
    }

    var enumValues = FilterEnum.values;
    var listFilters = converted.split(',').map<FilterEnum>(
        (x) => enumValues.firstWhere((enumValue) => enumValue.toString() == x));

    return listFilters.toSet();
  }

  static String _serializeIds(List<int> ids) {
    return ids != null ? ids.join(',') : null;
  }

  static String _serializeFilters(Set<FilterEnum> filters) {
    return filters != null ? filters.join(',') : null;
  }

  String getShortId() {
    List<String> uuidParts = uuid.split('-');
    return '${uuidParts[0]}-${uuidParts[1]}';
  }

  bool isReportCreationAvailable() {
    return selectedPurposeIds.contains(7);
  }

  bool isReportListAvailable() {
    return selectedPurposeIds.contains(8);
  }

  bool isReportStatusAvailable() {
    return selectedPurposeIds.contains(9);
  }

  bool isReportStatisticAvailable() {
    return selectedPurposeIds.contains(6);
  }

  // InfoMap
  bool isMapShowCurrentLocation() {
    return selectedPurposeIds.contains(1);
  }

  bool isMapShowStatistic() {
    return selectedPurposeIds.contains(6);
  }

  bool isMapShowLocationGroups() {
    return selectedPurposeIds.contains(2);
  }

  bool isMapUsePredefinedLocationGroups() {
    return selectedPurposeIds.contains(3);
  }

  bool isMapLeaveReviewAndAssignScore() {
    return selectedPurposeIds.contains(4);
  }

  bool isMapShowReview() {
    return selectedPurposeIds.contains(5);
  }

  //Profile
  bool isPrefillAddress() {
    return selectedPurposeIds.contains(11);
  }

  bool isPrefillFirstLastName() {
    return selectedPurposeIds.contains(12);
  }

  bool isPrefillAvatar() {
    return selectedPurposeIds.contains(13);
  }

  bool isPurchaseAvailable() {
    return selectedPurposeIds.contains(10);
  }

  bool isNoConsent() {
    return selectedTemplateId == 1;
  }
}

class ConsentTemplate {
  final int id;
  final String name;
  final String description;
  final List<int> purposeIds;
  final bool isCustomisable;
  final bool isUsageBased;
  final int orderIndex;

  ConsentTemplate({
    this.id,
    this.name,
    this.description,
    this.purposeIds,
    this.isCustomisable,
    this.isUsageBased,
    this.orderIndex,
  });

  ConsentTemplate copyWith({
    id = Symbol.empty,
    name = Symbol.empty,
    description = Symbol.empty,
    isCustomisable = Symbol.empty,
    isUsageBased = Symbol.empty,
    orderIndex = Symbol.empty,
    purposeIds = Symbol.empty,
  }) {
    return ConsentTemplate(
      id: id != Symbol.empty ? id : this.id,
      name: name != Symbol.empty ? name : this.name,
      description: description != Symbol.empty ? description : this.description,
      isCustomisable:
          isCustomisable != Symbol.empty ? isCustomisable : this.isCustomisable,
      isUsageBased:
          isUsageBased != Symbol.empty ? isUsageBased : this.isUsageBased,
      orderIndex: orderIndex != Symbol.empty ? orderIndex : this.orderIndex,
      purposeIds: purposeIds != Symbol.empty ? purposeIds : this.purposeIds,
    );
  }

  static fromMap(Map<String, dynamic> data) => ConsentTemplate(
        id: data['id'],
        name: data['name'],
        description: data['description'],
        orderIndex: data['orderIndex'],
        isCustomisable: data['isCustomisable'],
        isUsageBased: data['isUsageBased'],
        purposeIds: data['purposeIds'].map<int>((x) => x as int).toList(),
      );
}

import 'dart:io';

import 'package:demo1/enums/report_status_enum.dart';
import 'package:demo1/models/location.dart';
import 'package:demo1/models/model.dart';

class Report extends Model {
  static String table = 'report';

  final int id;
  final int score;
  final String firstName;
  final String lastName;
  final String description;
  final String address;
  final File image;
  final String statusMessage;
  final ReportStatusEnum status;
  final Location location;
  final DateTime timestamp;

  Report({
    this.id,
    this.score,
    this.firstName,
    this.lastName,
    this.description,
    this.address,
    this.image,
    this.statusMessage,
    this.status,
    this.location,
    this.timestamp,
  });

  Report copyWith({
    id = Symbol.empty,
    score = Symbol.empty,
    firstName = Symbol.empty,
    lastName = Symbol.empty,
    description = Symbol.empty,
    address = Symbol.empty,
    image = Symbol.empty,
    statusMessage = Symbol.empty,
    status = Symbol.empty,
    location = Symbol.empty,
    timestamp = Symbol.empty,
  }) {
    return Report(
      id: id != Symbol.empty ? id : this.id,
      score: score != Symbol.empty ? score : this.score,
      firstName: firstName != Symbol.empty ? firstName : this.firstName,
      lastName: lastName != Symbol.empty ? lastName : this.lastName,
      description: description != Symbol.empty ? description : this.description,
      address: address != Symbol.empty ? address : this.address,
      image: image != Symbol.empty ? image : this.image,
      statusMessage:
          statusMessage != Symbol.empty ? statusMessage : this.statusMessage,
      status: status != Symbol.empty ? status : this.status,
      location: location != Symbol.empty ? location : this.location,
      timestamp: timestamp != Symbol.empty ? timestamp : this.timestamp,
    );
  }

  static fromMap(Map<String, dynamic> data) {
    return Report(
      id: data['id'],
      score: data['score'],
      firstName: data['firstName'].toString(),
      lastName: data['lastName'].toString(),
      description: data['description'].toString(),
      address: data['address'].toString(),
      image: File(data['imageUri']),
      statusMessage: data['statusMessage'],
      status: getStatus(data['status']),
      location: getLocation(data['latitude'], data['longitude']),
      timestamp: DateTime.fromMillisecondsSinceEpoch(data['timestamp']),
    );
  }

  Map<String, dynamic> toMap() => {
        "id": this.id,
        "score": this.score,
        "firstName": this.firstName,
        "lastName": this.lastName,
        "description": this.description,
        "address": this.address,
        "imageUri": this.image.path,
        "statusMessage": this.statusMessage,
        "status": this.status?.toString(),
        "latitude": this.location?.latitude,
        "longitude": this.location?.longitude,
        "timestamp": this.timestamp.millisecondsSinceEpoch,
      };

  static ReportStatusEnum getStatus(String value) {
    if (value == null) {
      return null;
    }
    var values = ReportStatusEnum.values;
    return values.firstWhere((e) => e.toString() == value);
  }

  static Location getLocation(double latitude, double longitude) {
    return Location(latitude: latitude, longitude: longitude);
  }

  static String getStatusString(ReportStatusEnum status) {
    switch (status) {
      case ReportStatusEnum.submitted:
        return 'submitted';
      case ReportStatusEnum.inProgress:
        return 'in progress';
      case ReportStatusEnum.rejected:
        return 'unsolvable';
      case ReportStatusEnum.done:
        return 'done';
    }

    return 'N/A';
  }
}

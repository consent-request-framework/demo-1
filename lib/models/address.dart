class Address {
  final String street;
  final int zipCode;
  final String city;

  Address({
    this.street,
    this.zipCode,
    this.city,
  });

  Address copyWith({
    street = Symbol.empty,
    zipCode = Symbol.empty,
    city = Symbol.empty,
  }) {
    return Address(
      street: street != Symbol.empty ? street : this.street,
      zipCode: zipCode != Symbol.empty ? zipCode : this.zipCode,
      city: city != Symbol.empty ? city : this.city,
    );
  }
}

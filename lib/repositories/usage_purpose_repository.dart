import 'package:demo1/database/database.dart';
import 'package:demo1/models/usage_purpose.dart';

class UsagePurposeRepository {
  List<UsagePurpose> _items = [];

  List<UsagePurpose> get items {
    return [... _items];
  }

  Future<void> init() async {
    final usageDbResult = await DB.query(UsagePurpose.table);
    _items = usageDbResult
        .map<UsagePurpose>((x) => UsagePurpose.fromMap(x))
        .toList();
  }

  bool isReady() {
    final reports = _items.where((e) => e.purposeId == 7).length > 0;
    final reviews = _items.where((e) => e.purposeId == 4).length > 0;
    final tickets = _items.where((e) => e.purposeId == 10).length > 0;
    final avatars = _items.where((e) => e.purposeId == 13).length > 0;
    final stats = _items.where((e) => e.purposeId == 6).length > 0;

    print('reports: $reports, reviews: $reviews, tickets: $tickets, avatars: $avatars, stats: $stats');

    return reports && reviews && tickets && avatars && stats;
  }

  Future<void> removeAll(bool keepAvatar) async {
    int count = await DB.rawDelete('DELETE FROM ${UsagePurpose.table}');
    _items = [];
    if (keepAvatar) {
      avatarIsAdded();
    }
    print('deleted usage: $count');
  }

  Future<void> currentLocationIsShowed() async {
    await _insert(1);
  }

  Future<void> locationGroupsIsShowed() async {
    await _insert(2);
  }

  Future<void> locationGroupsFromPrefIsUsed() async {
    await _insert(3);
  }

  Future<void> ownReviewIsShown() async {
    await _insert(5);
  }

  Future<void> listReportsIsShown() async {
    await _insert(8);
  }

  Future<void> reportStatusIsShown() async {
    await _insert(9);
  }

  Future<void> homeAddressIsAutofilled() async {
    await _insert(11);
  }

  Future<void> fullNameIsAutofilled() async {
    await _insert(12);
  }

  // ---

  Future<void> reviewIsAdded() async {
    await _insert(4);
  }

  Future<void> reportIsAdded() async {
    await _insert(7);
  }

  Future<void> ticketIsAdded() async {
    await _insert(10);
  }

  Future<void> avatarIsAdded() async {
    await _insert(13);
  }

  Future<void> statisticIsViewed() async {
    await _insert(6);
  }

  Future<void> _insert(int purposeId) async {
    final purpose = UsagePurpose(
      purposeId: purposeId,
      timestamp: DateTime.now(),
    );
    _items.add(purpose.copyWith());
    await DB.insert(UsagePurpose.table, purpose);
  }
}

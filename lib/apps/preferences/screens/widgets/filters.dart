import 'package:demo1/enums/filter_group_enum.dart';
import 'package:demo1/models/profile.dart';
import 'package:demo1/utils/enum_to_string.dart';
import 'package:flutter/material.dart';

class Filters extends StatefulWidget {
  final FilterEnum filter;
  final Function onPreferenceSelect;
  final Profile profile;

  Filters(this.filter, this.onPreferenceSelect, this.profile);

  @override
  _FiltersState createState() =>
      _FiltersState(profile.isFilterSelected(filter));
}

class _FiltersState extends State<Filters> {
  bool _isSelected;

  _FiltersState(this._isSelected);

  _onTap() {
    setState(() {
      _isSelected = !_isSelected;
    });
    widget.onPreferenceSelect(widget.filter, _isSelected);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(15.0),
        onTap: _onTap,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(50.0),
                child: Image.asset(
                  FilterUtil.getIconPath(widget.filter, _isSelected),
                  height: 60.0,
                  width: 60.0,
                  fit: BoxFit.scaleDown,
                ),
              ),
              SizedBox(width: 15),
              Text(
                FilterUtil.getTitle(widget.filter),
                style: TextStyle(fontSize: 20),
              ),
              Expanded(
                child: _isSelected
                    ? Container(
                        alignment: Alignment.centerRight,
                        child: ImageIcon(
                          AssetImage('assets/images/active_icons.png'),
                          size: 25,
                          color: Colors.black54,
                        ),
                      )
                    : Container(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

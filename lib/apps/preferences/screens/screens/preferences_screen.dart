import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/common/widgets/list_header.dart';
import 'package:demo1/enums/filter_group_enum.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/filters.dart';

class PreferencesScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;

    Set<FilterEnum> preselectedFilters = profile.selectedMapFilters.length > 0
        ? Set.of(profile.selectedMapFilters)
        : Set();

    _onPreferenceSelect(FilterEnum filter, bool isSelected) {
      isSelected
          ? preselectedFilters.add(filter)
          : preselectedFilters.remove(filter);
    }

    _onSave() async {
      if (profile.isMapUsePredefinedLocationGroups()) {
        final profileModel = profile.copyWith(
          selectedMapFilters: Set.of(preselectedFilters),
        );
        await Provider.of<ProfileProvider>(context, listen: false)
            .saveProfile(profileModel);
        final provider =
            Provider.of<ReviewListProvider>(context, listen: false);
        provider.initPreselection(profileModel.selectedMapFilters);
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Data has been saved'),
          duration: Duration(milliseconds: 2000),
        ));
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
              'Sorry, you haven\'t given consent to process your data to use preferences'),
          duration: Duration(milliseconds: 2000),
        ));
      }

      final app = Provider.of<AppProvider>(context, listen: false);
      app.usageRepo.locationGroupsFromPrefIsUsed();
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Preferences'),
        centerTitle: true,
        leading: IconButton(
          icon: new Icon(Icons.close),
          onPressed: () => {
            Provider.of<AppProvider>(context, listen: false).goBack(),
          },
        ),
      ),
      body: profile.isMapUsePredefinedLocationGroups()
          ? ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 12,
                    bottom: 20,
                  ),
                  child: ListHeader('Pick your map preferences:'),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Filters(FilterEnum.park, _onPreferenceSelect, profile),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Filters(
                      FilterEnum.restaurant, _onPreferenceSelect, profile),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Filters(FilterEnum.bike, _onPreferenceSelect, profile),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Filters(FilterEnum.poi, _onPreferenceSelect, profile),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      child: Text(
                        'Save',
                        style: mainButtonTextStyle,
                      ),
                      onPressed: _onSave,
                    ),
                  ],
                ),
              ],
            )
          : ConsentMessage.mapUsePredefinedLocationGroups(context),
    );
  }
}

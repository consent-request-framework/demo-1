import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/screens/preferences_screen.dart';

class PreferencesApp extends StatelessWidget {
  const PreferencesApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('PreferencesApp');
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case PreferencesScreen.routeName:
                return PreferencesScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}

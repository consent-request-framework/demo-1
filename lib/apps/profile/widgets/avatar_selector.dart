import 'dart:io';

import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:provider/provider.dart';

class AvatarSelector extends StatefulWidget {
  final Function onSelectImage;

  AvatarSelector(this.onSelectImage);

  @override
  _AvatarSelectorState createState() => _AvatarSelectorState();
}

class _AvatarSelectorState extends State<AvatarSelector> {
  File _storedImage;
  var _isInit = false;

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      _storedImage =
          Provider.of<ProfileProvider>(context, listen: false).profile.avatar;
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  _deletePicture() {
    setState(() {
      _storedImage = null;
      widget.onSelectImage(null);
    });
  }

  Future<void> _takePicture() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.getImage(
        source: ImageSource.camera,
        maxWidth: 600
    );
    if (imageFile == null) {
      return;
    }
    final file = File(imageFile.path);
    setState(() {
      _storedImage = file;
    });
    final appDir = await syspaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    final savedImage = await file.copy('${appDir.path}/$fileName');
    widget.onSelectImage(savedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          InkWell(
            borderRadius: BorderRadius.all(Radius.circular(50)),
            onTap: _takePicture,
            child: _storedImage == null
                ? Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 15,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Image(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/images/upload_photo.png'),
                      ),
                    ),
                  )
                : Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.grey.withOpacity(0.8)
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 15, // changes position of shadow
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: Image(
                        fit: BoxFit.cover,
                        width: 100,
                        height: 100,
                        image: FileImage(_storedImage),
                      ),
                    ),
                  ),
          ),
          SizedBox(
            height: 10,
          ),
          _storedImage != null
              ? FlatButton(
                  child: Text('Delete Profile Picture'),
                  onPressed: _deletePicture,
                )
              : Text('Upload your profile picture')
        ],
      ),
    );
  }
}

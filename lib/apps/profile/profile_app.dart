import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/profile_screen.dart';

class ProfileApp extends StatelessWidget {
  const ProfileApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('ProfileApp');
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case ProfileScreen.routeName:
                return ProfileScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}

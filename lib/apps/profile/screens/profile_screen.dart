import 'package:demo1/apps/profile/widgets/avatar_selector.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/models/profile.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _form = GlobalKey<FormState>();
  final _firstNameFocusNode = FocusNode();
  final _lastNameFocusNode = FocusNode();
  final _streetFocusNode = FocusNode();
  final _zipCodeFocusNode = FocusNode();
  final _cityFocusNode = FocusNode();
  var _model = Profile();
  var _isInit = false;

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      _model = Provider.of<ProfileProvider>(context, listen: false).profile;
      _isInit = true;
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _firstNameFocusNode.dispose();
    _lastNameFocusNode.dispose();
    _streetFocusNode.dispose();
    _zipCodeFocusNode.dispose();
    _cityFocusNode.dispose();
    super.dispose();
  }

  Future<void> _saveForm() async {
    _form.currentState.save();

    await Provider.of<ProfileProvider>(context, listen: false).saveProfile(
      _model.copyWith(),
    );
    Scaffold.of(context).showSnackBar(_snackBar);

    final app = Provider.of<AppProvider>(context, listen: false);
    if (_model.address?.city != null ||
        _model.address?.street != null ||
        _model.address?.zipCode != null) {
      app.usageRepo.homeAddressIsAutofilled();
    }
    if (_model.firstName != null || _model.lastName != null) {
      app.usageRepo.fullNameIsAutofilled();
    }
  }

  final _snackBar = SnackBar(
    content: Text('Data has been saved  '),
    duration: Duration(milliseconds: 2000),
  );

  @override
  Widget build(BuildContext context) {
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        centerTitle: true,
        leading: IconButton(
          icon: new Icon(Icons.close),
          onPressed: () => {
            Provider.of<AppProvider>(context, listen: false).goBack(),
          },
        ),
      ),
      body: profile.isPrefillAvatar() ||
              profile.isPrefillAddress() ||
              profile.isPrefillFirstLastName()
          ? Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Form(
                        key: _form,
                        child: Column(
                          children: <Widget>[
                            profile.isPrefillAvatar()
                                ? AvatarSelector(
                                    (file) => {
                                      _model = _model.copyWith(avatar: file),
                                    },
                                  )
                                : ConsentMessage.profileNoAvatar(context),
                            SizedBox(height: 20),
                            profile.isPrefillFirstLastName()
                                ? Column(
                                    children: <Widget>[
                                      TextFormField(
                                        decoration: InputDecoration(
                                          labelText: 'First Name',
                                        ),
                                        initialValue: _model.firstName ?? '',
                                        focusNode: _firstNameFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onFieldSubmitted: (_) {
                                          FocusScope.of(context)
                                              .requestFocus(_lastNameFocusNode);
                                        },
                                        onSaved: (value) {
                                          _model =
                                              _model.copyWith(firstName: value);
                                        },
                                      ),
                                      SizedBox(height: 10),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          labelText: 'Last Name',
                                        ),
                                        initialValue: _model.lastName ?? '',
                                        focusNode: _lastNameFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onFieldSubmitted: (_) {
                                          FocusScope.of(context)
                                              .requestFocus(_streetFocusNode);
                                        },
                                        onSaved: (value) {
                                          _model =
                                              _model.copyWith(lastName: value);
                                        },
                                      ),
                                    ],
                                  )
                                : ConsentMessage.profileNoFullName(context),
                            SizedBox(height: 15),
                            profile.isPrefillAddress()
                                ? Column(
                                    children: <Widget>[
                                      Text(
                                        'Address',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6
                                            .copyWith(
                                              fontWeight: FontWeight.bold,
                                            ),
                                      ),
                                      SizedBox(height: 15),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          labelText: 'Street and House Number',
                                        ),
                                        initialValue:
                                            _model.address?.street ?? '',
                                        focusNode: _streetFocusNode,
                                        textInputAction: TextInputAction.next,
                                        onFieldSubmitted: (_) {
                                          FocusScope.of(context)
                                              .requestFocus(_zipCodeFocusNode);
                                        },
                                        onSaved: (value) {
                                          _model = _model.copyWith(
                                            address: _model.address
                                                .copyWith(street: value),
                                          );
                                        },
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        children: <Widget>[
                                          Flexible(
                                            child: TextFormField(
                                              decoration: InputDecoration(
                                                labelText: 'Zip Code',
                                              ),
                                              initialValue: _model
                                                      .address?.zipCode
                                                      ?.toString() ??
                                                  '',
                                              focusNode: _zipCodeFocusNode,
                                              textInputAction:
                                                  TextInputAction.next,
                                              onFieldSubmitted: (_) {
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        _cityFocusNode);
                                              },
                                              keyboardType:
                                                  TextInputType.number,
                                              onSaved: (value) {
                                                if (value == null ||
                                                    value == '') {
                                                  return;
                                                }
                                                _model = _model.copyWith(
                                                  address:
                                                      _model.address.copyWith(
                                                    zipCode: int.parse(value),
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Flexible(
                                            child: TextFormField(
                                              decoration: InputDecoration(
                                                labelText: 'City',
                                              ),
                                              initialValue:
                                                  _model.address?.city ?? '',
                                              focusNode: _cityFocusNode,
                                              textInputAction:
                                                  TextInputAction.done,
                                              onSaved: (value) {
                                                _model = _model.copyWith(
                                                  address: _model.address
                                                      .copyWith(city: value),
                                                );
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )
                                : ConsentMessage.profileNoAddress(context),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    alignment: Alignment.center,
                    child: RaisedButton(
                      child:
                          Text('Save', style: TextStyle(color: colorDarkRed)),
                      onPressed: _saveForm,
                    ),
                  ),
                ],
              ),
            )
          : ConsentMessage.profileNoConsent(context),
    );
  }
}

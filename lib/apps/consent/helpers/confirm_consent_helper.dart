import 'package:demo1/apps/consent/providers/purpose_list_provider.dart';
import 'package:demo1/apps/consent/providers/template_list_provider.dart';
import 'package:demo1/apps/consent/widgets/consent_item.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/modal_dialog.dart';
import 'package:demo1/common/widgets/list_header.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConfirmConsentHelper {
  static Future<void> showConfirmMessage(BuildContext context) async {
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;
    final template = Provider.of<TemplateListProvider>(context, listen: false);
    await template.fetch(context);

    final tpl = template.findById(profile.selectedTemplateId);
    final purpose = Provider.of<PurposeListProvider>(context, listen: false);

    await purpose.fetch(context, tpl);

    final app = Provider.of<AppProvider>(context, listen: false);

    Future<void> _selectTemplate() async {
      var areYouSure = await ModalDialog.showAreYouSure(
        context,
        body: Text('Are you sure you want to give consent?'),
        yesBtn: 'Yes, I\'m Sure',
        noBtn: 'Cancel',
      );

      if (areYouSure) {
        await purpose.saveSelection(context, tpl.id);
        Navigator.of(context).pop();
      }
    }

    Navigator.of(context).push(
      MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              appBar: AppBar(
                title: Text('Personalized functionality'),
                centerTitle: true,
                leading: Container(),
              ),
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: ListHeader('This consent request for personal data processing has been generated for you based on your app usage.'),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: purpose.items.length,
                      itemBuilder: (BuildContext ctx, int i) {
                        return ConsentItem(purpose.items[i], null);
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          offset: Offset(0.0, -3.0), //(x,y)
                          blurRadius: 2.0,
                          spreadRadius: 0.05,
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          child: Text('Change Consent',
                              style: mainButtonTextStyle),
                          onPressed: () {
                            Navigator.of(context).pop();
                            app.goToConsent();
                          },
                        ),
                        Spacer(),
                        RaisedButton(
                          child: Text('I Agree', style: mainButtonTextStyle),
                          onPressed: _selectTemplate,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

import 'package:cure_component/graph/config.dart';
import 'package:cure_component/graph/graph.model.dart';
import 'package:cure_component/graph/graph.painter.dart';
import 'package:demo1/apps/consent/providers/purpose_list_provider.dart';
import 'package:demo1/apps/consent/providers/template_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomSheetHelper {
  static void showGraph(BuildContext context, int purposeId) {
    var purposePrd = Provider.of<PurposeListProvider>(context, listen: false);
    var templatePrd = Provider.of<TemplateListProvider>(context, listen: false);
    final purpose = purposePrd.items.singleWhere((x) => x.id == purposeId);
    final legend = Map<IconData, String>();
    templatePrd.activities.forEach((e) => {legend[e.icon] = e.label});

    final List<GraphItem> graphItems = List();
    purpose.activities.forEach((activity) {
      final graphItem = GraphItem(
          target: templatePrd.getProcessorById(activity.processor).label,
          source: Map<IconData, String>(),
          connections: Set<IconData>()
      );
      var dataItems = [...purpose.getData()];
      var dataLegend = templatePrd.getData();
      dataItems.sort((a, b) => a.compareTo(b));
      for (final dataItem in dataItems) {
        final item =
        dataLegend.singleWhere((element) => element.id == dataItem);
        graphItem.source[item.icon] = item.label;
      }

      templatePrd.activities.forEach((activityLegend) {
        if (activity.processorActivity.containsKey(activityLegend.id)) {
          graphItem.connections.add(activityLegend.icon);
        }
      });

      graphItems.add(graphItem);
    });

    final config = GraphConfig(
      GraphModel(
        name: purpose.name,
        legend: legend,
        items: graphItems,
      ),
    );

    var graphHeight = config.getHeight() + 25;
    var maxHeight = MediaQuery.of(context).size.height * 0.90;
    if (graphHeight > maxHeight) {
      graphHeight = maxHeight;
    }

    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        height: graphHeight,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(25.0),
            topRight: const Radius.circular(25.0),
          ),
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            Container(
              width: 90,
              child: Image.asset('assets/images/ic_grey_line.png'),
            ),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomPaint(
                      size: Size(config.getWidth(), config.getHeight()),
                      painter: GraphPainter(config: config),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

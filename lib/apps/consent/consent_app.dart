import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/purpose_list_screen.dart';
import 'screens/template_list_screen.dart';

class ConsentApp extends StatelessWidget {
  const ConsentApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('ConsentApp');
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case TemplateListScreen.routeName:
                return TemplateListScreen();
              case PurposeListScreen.routeName:
                return PurposeListScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}

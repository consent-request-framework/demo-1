import 'package:demo1/apps/consent/providers/template_list_provider.dart';
import 'package:demo1/apps/consent/widgets/consent_list.dart';
import 'package:demo1/apps/consent/widgets/template_list.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TemplateListScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Consent'),
        centerTitle: true,
        leading: Consumer<ProfileProvider>(
          builder: (ctx, profile, ch) => profile.hasConsent
              ? IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () => {
                    Provider.of<AppProvider>(ctx, listen: false).goBack(),
                  },
                )
              : Container(width: 0.0, height: 0.0),
        ),
      ),
      body: FutureBuilder(
        future:
            Provider.of<TemplateListProvider>(ctx, listen: false).fetch(ctx),
        builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
            ? Center(child: CircularProgressIndicator())
            : Consumer<TemplateListProvider>(
                child: Center(child: Text('Please reinstall the app.')),
                builder: (ctx, data, noTemplates) {
                  final items = data.items;
                  if (items.length == 0) {
                    return noTemplates;
                  }
                  if (items.length == 1 && items[0].isCustomisable) {
                    return ConsentList(items[0], false);
                  }
                  return TemplateList(items);
                },
              ),
      ),
    );
  }
}

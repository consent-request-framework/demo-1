import 'package:demo1/apps/consent/providers/template_list_provider.dart';
import 'package:demo1/apps/consent/widgets/consent_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PurposeListScreen extends StatelessWidget {
  static const routeName = '/purpose-list-screen';

  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context).settings.arguments;
    final provider = Provider.of<TemplateListProvider>(context, listen: false);
    final template = provider.findById(id);

    return Scaffold(
      appBar: AppBar(
        title: Text(template.name),
        centerTitle: true,
      ),
      body: ConsentList(template, true),
    );
  }
}

import 'dart:convert';
import 'dart:core';

import 'package:cure_component/models/data.dart';
import 'package:demo1/models/consent_template.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class TemplateListProvider with ChangeNotifier {
  List<ConsentTemplate> _templates = [];
  List<Data> _activities = [];
  List<Data> _processors = [];
  List<Data> _data = [];

  List<ConsentTemplate> get items {
    return [..._templates];
  }

  List<Data> get activities {
    return [..._activities];
  }

  List<Data> getData() {
    return [..._data];
  }

  Data getProcessorById(String id) {
    return _processors.singleWhere((x) => x.id == id).copyWith();
  }

  ConsentTemplate findById(int id) {
    return _templates.firstWhere((x) => x.id == id);
  }

  Future<void> fetch(ctx) async {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    final data = await rootBundle.loadString('assets/jsons/consent_data.json');
    final jsonResult = json.decode(data);
    final List<ConsentTemplate> allTemplates = [];
    for (final item in jsonResult['templates']) {
      allTemplates.add(ConsentTemplate.fromMap(item));
    }

    final url = 'https://cure.wu.ac.at/templates?user=${profile.uuid}';
    final response = await http.get(url);
    final jsonResponse = json.decode(response.body);
    final group = json.decode(jsonResponse['numbersArray']['group']);
    final assignedTemplates = group[0];

    _templates = [];
    for (final template in allTemplates) {
      if (assignedTemplates.contains(template.id)) {
        _templates.add(template);
      }
    }

   // _templates.add(allTemplates.firstWhere((element) => element.id == 1));
   // _templates.add(allTemplates.firstWhere((element) => element.id == 8));
   // _templates.add(allTemplates.firstWhere((element) => element.id == 9));

    _activities = [];
    for (final item in jsonResult['activities']) {
      _activities.add(Data.fromMap(item));
    }

    _processors = [];
    for (final item in jsonResult['processors']) {
      _processors.add(Data.fromMap(item));
    }

    _data = [];
    for (final item in jsonResult['data']) {
      _data.add(Data.fromMap(item));
    }

    notifyListeners();
  }
}

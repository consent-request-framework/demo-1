import 'dart:convert';
import 'dart:core';

import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/common/helpers/modal_dialog.dart';
import 'package:demo1/enums/filter_group_enum.dart';
import 'package:demo1/models/address.dart';
import 'package:demo1/models/consent_template.dart';
import 'package:demo1/models/purpose.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'template_list_provider.dart';

class PurposeListProvider with ChangeNotifier {
  final UsagePurposeRepository usageRepo;

  List<Purpose> _purposes = [];

  PurposeListProvider(this.usageRepo);

  List<Purpose> get items {
    return [..._purposes];
  }

  Future<void> fetch(BuildContext ctx, ConsentTemplate template) async {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    List<int> selectedPurposeIds = [...profile.selectedPurposeIds];
    List<int> templatePurposeIds = [...template.purposeIds];
    final allPurposes = await _loadPurposes();
    if (template.id != profile.selectedTemplateId) {
      if (template.isCustomisable) {
        selectedPurposeIds = []; //...profile.selectedPurposeIds
      } else {
        selectedPurposeIds = [...templatePurposeIds];
      }
    } else if (template.isUsageBased) {
      if (usageRepo.isReady()) {
        final Set<int> usageIds =
            usageRepo.items.map((e) => e.purposeId).toSet();
        final Set<int> generatedPurposes = {};
        for (int purposeId in usageIds) {
          _findAllParentIds(purposeId, generatedPurposes, allPurposes);
        }
        templatePurposeIds = [...generatedPurposes];
        selectedPurposeIds = [...generatedPurposes];
      } else {
        templatePurposeIds = [...template.purposeIds];
        selectedPurposeIds = [...template.purposeIds];
      }
    }

    _purposes = [];
    for (final purpose in allPurposes) {
      if (!templatePurposeIds.contains(purpose.id)) {
        continue;
      }
      _purposes.add(
        purpose.copyWith(
          isSelectable: template.isCustomisable,
          isSelected: selectedPurposeIds.contains(purpose.id),
        ),
      );
    }

    notifyListeners();
  }

  Future<void> update(context, int purposeId, bool isSelected) async {
    Set<int> siblingIds = {};
    (isSelected)
        ? _findAllParentIds(purposeId, siblingIds, null)
        : _findAllChildIds(purposeId, siblingIds);
    siblingIds.remove(purposeId);

    List<int> selectionIds = _purposes
        .where((x) => x.isSelected == isSelected)
        .map((e) => e.id)
        .toList();

    siblingIds.removeAll(selectionIds);

    final siblings = _findPurposesByIds(siblingIds.toList());
    siblings.sort((a, b) => a.id.compareTo(b.id));

    var areYouSure = true;
    if (siblings.length > 0) {
      areYouSure = await _areYouSure(context, siblings, isSelected);
    }

    if (!areYouSure) {
      return;
    }

    List<int> purposesToUpdate = siblings.map((e) => e.id).toList();
    purposesToUpdate.add(purposeId);
    for (var i = 0; i < _purposes.length; i++) {
      final purpose = _purposes[i];
      if (purposesToUpdate.contains(purpose.id)) {
        _purposes[i] = purpose.copyWith(isSelected: isSelected);
      }
    }

    notifyListeners();
  }

  bool isConfirmed(int templateId, ctx) {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    final templatePrv = Provider.of<TemplateListProvider>(ctx, listen: false);
    ConsentTemplate template = templatePrv.findById(templateId);
    if (template.isCustomisable) {
      return false;
    }

    if (!profile.isTemplateSelected(templateId)) {
      return false;
    }

    if (template.isUsageBased &&
        usageRepo.isReady() &&
        !profile.isConsentConfirmed) {
      return false;
    }

    return true;
  }

  Future<void> saveSelection(ctx, int templateId) async {
    final templatePrv = Provider.of<TemplateListProvider>(ctx, listen: false);
    final profileProvider = Provider.of<ProfileProvider>(ctx, listen: false);
    List<Purpose> purposes = [..._purposes];
    ConsentTemplate template = templatePrv.findById(templateId);
    if (template.isCustomisable || template.isUsageBased) {
      purposes = _purposes.where((purpose) => purpose.isSelected).toList();
    }

    final currentProfile = profileProvider.profile;
    if (template.isUsageBased &&
        !currentProfile.isTemplateSelected(templateId)) {
      await usageRepo.removeAll(currentProfile.avatar != null);
      profileProvider.initTimer();
    }

    final selectedPurposeIds = purposes.map((x) => x.id).toList();
    final selectedMapFilters = currentProfile.selectedMapFilters;
    final profile = currentProfile.copyWith(
        isConsentConfirmed: template.isUsageBased &&
            currentProfile.isTemplateSelected(templateId),
        selectedTemplateId: templateId,
        templates: templatePrv.items.map((e) => e.id).join('-'),
        selectedPurposeIds: selectedPurposeIds,
        avatar:
            (selectedPurposeIds.contains(13)) ? currentProfile.avatar : null,
        firstName:
            (selectedPurposeIds.contains(12)) ? currentProfile.firstName : null,
        lastName:
            (selectedPurposeIds.contains(12)) ? currentProfile.lastName : null,
        address:
            (selectedPurposeIds.contains(11)) ? currentProfile.address : Address(),
        selectedMapFilters: (selectedPurposeIds.contains(3))
            ? selectedMapFilters
            : Set<FilterEnum>());

    final provider = Provider.of<ReviewListProvider>(ctx, listen: false);
    provider.initPreselection(profile.selectedMapFilters);

    await profileProvider.saveProfile(profile);
    templatePrv.notifyListeners();
  }

  Future<bool> _areYouSure(
    context,
    List<Purpose> purposeRefs,
    bool isSelected,
  ) {
    final selected = isSelected ? 'selected' : 'deselected';
    return ModalDialog.showAreYouSure(
      context,
      body: RichText(
        textAlign: TextAlign.left,
        text: TextSpan(
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 16.0,
            color: Colors.black,
          ),
          children: <TextSpan>[
            TextSpan(text: 'The following purposes also will be $selected:\n'),
            TextSpan(
              text: purposeRefs.map((e) => e.name).join('\n'),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      yesBtn: isSelected ? 'Select' : 'Deselect',
      noBtn: 'Cancel',
    );
  }

  Future<List<Purpose>> _loadPurposes() async {
    final data = await rootBundle.loadString('assets/jsons/consent_data.json');
    final jsonResult = json.decode(data);
    final List<Purpose> result = [];
    for (final item in jsonResult['purposes']) {
      result.add(Purpose.fromMap(item));
    }
    return result;
  }

  void _findAllParentIds(int currentId, Set<int> res, List<Purpose> purposes) {
    res.add(currentId);
    final purpose =
        (purposes ?? _purposes).firstWhere((x) => x.id == currentId);
    for (final parentId in purpose.parentIds) {
      if (res.contains(parentId)) {
        continue;
      }
      _findAllParentIds(parentId, res, purposes);
    }
  }

  void _findAllChildIds(int currentId, Set<int> result) {
    result.add(currentId);
    final purpose = _purposes.firstWhere((x) => x.id == currentId);
    for (final childId in purpose.childIds) {
      if (result.contains(childId)) {
        continue;
      }
      _findAllChildIds(childId, result);
    }
  }

  List<Purpose> _findPurposesByIds(List<int> purposeIds) {
    List<Purpose> result = [];
    for (int purposeId in purposeIds) {
      final purpose = _purposes.firstWhere((x) => x.id == purposeId).copyWith();
      result.add(purpose);
    }
    return result;
  }
}

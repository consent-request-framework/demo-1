import 'package:demo1/apps/consent/helpers/bottom_sheet_helper.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/models/purpose.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ConsentItem extends StatelessWidget {
  final Purpose purpose;
  final Function onSelectItem;

  const ConsentItem(
    this.purpose,
    this.onSelectItem, {
    Key key,
  }) : super(key: key);

  List<Widget> _getDataWidgets() {
    final Map<String, IconData> icons = {
      "review": FontAwesomeIcons.solidComments,
      "reviewed place": FontAwesomeIcons.landmark,
      "phone id": FontAwesomeIcons.mobile,
      "location": FontAwesomeIcons.mapMarkerAlt,
      "location group": FontAwesomeIcons.ellipsisH,
      "full name": FontAwesomeIcons.idCard,
      "profile photo": FontAwesomeIcons.portrait,
      "photo": FontAwesomeIcons.camera,
      "issue description": FontAwesomeIcons.solidComment,
      "home address": FontAwesomeIcons.home,
    };

    List<Widget> result = [];
    var items = [... purpose.data];
    items.sort((a, b) => a.compareTo(b));
    for (final data in items) {
      result.add(
        Chip(
          label: Text(
            data,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          avatar: FaIcon(
            icons[data.toLowerCase()],
            size: 15,
            color: colorDarkRed,
          ),
          backgroundColor: Colors.transparent,
        ),
      );
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  purpose.isSelectable
                      ? Switch(
                          value: purpose.isSelected,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          activeColor: colorDarkRed,
                          onChanged: (value) {
                            onSelectItem(purpose.id, value);
                          },
                        )
                      : SizedBox(height: 40),
                  Expanded(
                    child: Text(
                      purpose.name.toUpperCase(),
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      child: ImageIcon(
                        AssetImage('assets/images/info_icon.png'),
                        size: 25,
                        color: Colors.black54,
                      ),
                      onTap: () {
                        BottomSheetHelper.showGraph(context, purpose.id);
                      },
                    ),
                  )
                ],
              ),
              Divider(color: Colors.black26),
              SizedBox(height: 5),
              Wrap(
                spacing: 6.0,
                runSpacing: -10.0,
                children: _getDataWidgets(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

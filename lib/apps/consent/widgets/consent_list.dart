import 'package:demo1/apps/consent/providers/purpose_list_provider.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/modal_dialog.dart';
import 'package:demo1/models/consent_template.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'consent_item.dart';

class ConsentList extends StatelessWidget {
  final bool isAutoClose;
  final ConsentTemplate template;

  const ConsentList(
    this.template,
    this.isAutoClose, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final purpose = Provider.of<PurposeListProvider>(ctx, listen: false);
    final app = Provider.of<AppProvider>(ctx, listen: false);
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;

    Future<void> _onPurposeSelect(int purposeId, bool isSelected) async {
      await purpose.update(ctx, purposeId, isSelected);
    }

    Future<void> _selectTemplate() async {
      String usage = '';
      if (template.id != 8 &&
          profile.selectedTemplateId == 8 &&
          app.usageRepo.isReady()) {
        usage =
            'Are you sure you want to change your "personalized functionality" consent to "${template.name}"?\n\nYour "personalized functionality" consent, generated based on your app usage, will be deleted.';
      } else {
        usage = 'Are you sure you want to give consent?';
      }

      var areYouSure = await ModalDialog.showAreYouSure(
        ctx,
        body: Text(usage),
        yesBtn: 'Yes, I\'m Sure',
        noBtn: 'Cancel',
      );

      if (areYouSure) {
        await purpose.saveSelection(ctx, template.id);

        if (isAutoClose) {
          Navigator.of(ctx).pop();
        } else {
          Scaffold.of(ctx).showSnackBar(SnackBar(
            content: Text('The consent has been saved'),
            duration: Duration(milliseconds: 2000),
          ));
        }
      }
    }

    return FutureBuilder(
      future: Provider.of<PurposeListProvider>(ctx, listen: false)
          .fetch(ctx, template),
      builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: <Widget>[
                Expanded(
                  child: Consumer<PurposeListProvider>(
                    builder: (ctx, purpose, ch) {
                      if (purpose.items.length == 0) {
                        return Center(
                          child: Text(
                            template.description,
                            textAlign: TextAlign.center,
                          ),
                        );
                      }
                      return ListView.builder(
                        itemCount: purpose.items.length,
                        itemBuilder: (BuildContext ctx, int i) {
                          return ConsentItem(
                              purpose.items[i], _onPurposeSelect);
                        },
                      );
                    },
                  ),
                ),
                purpose.isConfirmed(template.id, ctx)
                    ? Container()
                    : Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              offset: Offset(0.0, -3.0), //(x,y)
                              blurRadius: 2.0,
                              spreadRadius: 0.05,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: RaisedButton(
                              child:
                                  Text('I Agree', style: mainButtonTextStyle),
                              onPressed: _selectTemplate,
                            ),
                          ),
                        ),
                      ),
              ],
            ),
    );
  }
}

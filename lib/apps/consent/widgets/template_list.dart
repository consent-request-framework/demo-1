import 'package:demo1/common/widgets/list_header.dart';
import 'package:demo1/models/consent_template.dart';
import 'package:flutter/material.dart';

import 'template_item.dart';

class TemplateList extends StatelessWidget {
  final List<ConsentTemplate> templates;

  const TemplateList(
    this.templates, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (_, __) => SizedBox(height: 4),
      itemCount: templates.length + 1,
      itemBuilder: (ctx, i) {
        return Padding(
          padding: i == 0
              ? const EdgeInsets.only(top: 12, bottom: 20)
              : const EdgeInsets.only(left: 8.0, right: 8.0),
          child: i == 0
              ? Row(
                children: <Widget>[
                  ListHeader('Please choose your consent:'),
                  Spacer()
                ],
              )
              : TemplateItem(template: templates[i - 1]),
        );
      },
    );
  }
}

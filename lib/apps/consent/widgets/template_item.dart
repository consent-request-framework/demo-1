import 'package:demo1/apps/consent/screens/purpose_list_screen.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/models/consent_template.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TemplateItem extends StatelessWidget {
  final ConsentTemplate template;

  const TemplateItem({Key key, @required this.template}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    final app = Provider.of<AppProvider>(ctx, listen: false);

    Widget _getIcon(ConsentTemplate template) {
      var noIcon = SizedBox(height: 25);

      if (!profile.isTemplateSelected(template.id)) {
        return noIcon;
      }

      if (profile.selectedTemplateId == 8 &&
          app.usageRepo.isReady() &&
          !profile.isConsentConfirmed) {
        return noIcon;
      }

      return ImageIcon(
        AssetImage('assets/images/active_icons.png'),
        size: 25,
        color: Colors.black54,
      );
    }

    String _getDescription() {
      String description = template.description;
      if (template.id == 8 && profile.selectedTemplateId == 8 && app.usageRepo.isReady()) {
        description =
            'This consent request for personal data processing has been generated for you based on your app usage.';
      }

      return description;
    }

    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        onTap: () => {
          Navigator.pushNamed(
            ctx,
            PurposeListScreen.routeName,
            arguments: template.id,
          )
        },
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    template.name.toUpperCase(),
                    style: TextStyle(
                      color: colorDarkRed,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Spacer(),
                  _getIcon(template)
                ],
              ),
              Divider(color: Colors.black26),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 4),
                child: Text(_getDescription()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

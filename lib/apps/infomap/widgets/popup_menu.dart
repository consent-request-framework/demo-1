import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'popup_menu_info_button.dart';

class PopupMenu extends StatelessWidget {
  final Function onSelectionChange;

  const PopupMenu({
    Key key,
    this.onSelectionChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final profile = Provider.of<ProfileProvider>(context, listen: false).profile;
    final app = Provider.of<AppProvider>(context, listen: false);
    if (profile.isMapShowLocationGroups()) {
      app.usageRepo.locationGroupsIsShowed();
    }

    return PopupMenuButton<String>(
      icon: ImageIcon(
        AssetImage("assets/images/ic_filter_black.png"),
        color: Colors.black,
      ),
      itemBuilder: (BuildContext context) {
        return [
          PopupMenuItem(
            child: profile.isMapShowLocationGroups() ? Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: ImageIcon(
                      AssetImage("assets/images/ic_filter_red.png"),
                      color: Colors.red,
                    ),
                  ),
                ),
                Consumer<ReviewListProvider>(
                  builder: (ctx, data, ch) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        PopupMenuInfoButton(
                          label: 'Parks',
                          selected: 'assets/images/ic_parks_selected.png',
                          deselected: 'assets/images/ic_parks.png',
                          isSelected: data.parksSelected,
                          onTap: () {
                            data.parksSelected = !data.parksSelected;
                            onSelectionChange();
                          },
                        ),
                        PopupMenuInfoButton(
                          label: 'Restaurants',
                          selected: 'assets/images/ic_restaurants_selected.png',
                          deselected: 'assets/images/ic_restaurants.png',
                          isSelected: data.restSelected,
                          onTap: () {
                            data.restSelected = !data.restSelected;
                            onSelectionChange();
                          },
                        ),
                        PopupMenuInfoButton(
                          label: 'Bikes',
                          selected: 'assets/images/ic_bikes_selected.png',
                          deselected: 'assets/images/ic_bikes.png',
                          isSelected: data.bikesSelected,
                          onTap: () {
                            data.bikesSelected = !data.bikesSelected;
                            onSelectionChange();
                          },
                        ),
                        PopupMenuInfoButton(
                          label: 'POI',
                          selected: 'assets/images/ic_poi_selected.png',
                          deselected: 'assets/images/ic_poi.png',
                          isSelected: data.poiSelected,
                          onTap: () {
                            data.poiSelected = !data.poiSelected;
                            onSelectionChange();
                          },
                        ),
                      ],
                    );
                  },
                ),
              ],
            ) : ConsentMessage.mapNoLocationGroup(),
          ),
        ];
      },
    );
  }
}

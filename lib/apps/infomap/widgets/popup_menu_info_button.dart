import 'package:flutter/material.dart';

class PopupMenuInfoButton extends StatelessWidget {
  final String label;
  final String selected;
  final String deselected;
  final bool isSelected;
  final Function onTap;

  const PopupMenuInfoButton({
    Key key,
    this.label,
    this.selected,
    this.deselected,
    this.isSelected,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: onTap,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50.0),
            child: Image.asset(
              isSelected ? selected : deselected,
              height: 50,
              width: 50,
            ),
          ),
        ),
        SizedBox(height: 5.0),
        Text(
          label,
          style: TextStyle(
            color: Colors.black87.withOpacity(0.7),
            fontFamily: 'France',
            fontSize: 10,
            fontWeight: FontWeight.bold,
            letterSpacing: 1,
          ),
        ),
      ],
    );
  }
}

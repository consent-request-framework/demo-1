import 'dart:async';
import 'dart:typed_data';

import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/apps/infomap/screens/map_review_new_screen.dart';
import 'package:demo1/apps/infomap/screens/map_review_statistic_screen.dart';
import 'package:demo1/apps/infomap/widgets/popup_menu.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/common/widgets/progress_indicator.dart' as pi;
import 'package:demo1/helpers/google_helper.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_place/google_place.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MapScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  GoogleMapController _mController;

  bool _isLoading = true;
  LatLng _location;
  CameraPosition _cameraPosition =
      CameraPosition(target: LatLng(48.2082, 16.3738), zoom: 14.0000);
  Set<Marker> _markers = {};

  Future<void> _onMapCreated(GoogleMapController controller) async {
    setState(() {
      _isLoading = true;
    });
    controller.setMapStyle(
        '[{"featureType":"poi","stylers":[{"visibility": "off"}]}]');
    Completer<GoogleMapController> mapController = Completer();
    mapController.complete(controller);
    _mController = await mapController.future;

    await _setLocationAndCamera();
    await _buildMarkers();

    setState(() {
      _mController.moveCamera(CameraUpdate.newCameraPosition(_cameraPosition));
      _isLoading = false;
    });
  }

  Future<void> _onSelectionChange() async {
    setState(() {
      _isLoading = true;
    });

    await _buildMarkers();

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _setLocationAndCamera() async {
    _location = await GoogleHelper.getLocation(context);
    _cameraPosition = CameraPosition(target: _location, zoom: 14.0000);
  }

  Future<void> _buildMarkers() async {
    _markers = await GoogleHelper.getMarkers(context, _location, _showReview);
  }

  @override
  Widget build(BuildContext context) {
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;

    return Scaffold(
      appBar: AppBar(
        title: const Text('City Explorer'),
        centerTitle: true,
        actions: <Widget>[Avatar()],
        leading: IconButton(
          icon: ImageIcon(
            AssetImage("assets/images/ic_statistics.png"),
            color: Colors.black,
          ),
          onPressed: () => {
            Navigator.pushNamed(context, MapReviewStatisticScreen.routeName),
          },
        ),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            zoomControlsEnabled: false,
            zoomGesturesEnabled: true,
            mapToolbarEnabled: false,
            myLocationEnabled: profile.isMapShowCurrentLocation(),
            myLocationButtonEnabled: false,
            markers: _markers,
            initialCameraPosition: _cameraPosition,
            onMapCreated: _onMapCreated,
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 6.0)],
                  borderRadius: BorderRadius.circular(3),
                ),
                child: PopupMenu(onSelectionChange: _onSelectionChange),
              ),
            ),
          ),
          _isLoading ? pi.ProgressIndicator() : Container()
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _showReview(SearchResult result) async {
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;
    final reviewPrd = Provider.of<ReviewListProvider>(context, listen: false);
    DetailsResponse details;
    int reviewsSize;
    Uint8List image;

    Future<void> fetchData() async {
      final gPlace = GooglePlace(apiKey);
      details = await gPlace.details.get(result.placeId);
      var photos = details.result.photos;
      if (photos != null && photos.length > 0) {
        image = await gPlace.photos.get(photos[0].photoReference, null, 200);
      }
      reviewsSize = await reviewPrd.getUserReviewsSize(result.placeId);
      reviewsSize += (details.result.reviews?.length ?? 0);
    }

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      backgroundColor: Colors.white,
      builder: (BuildContext context) => FractionallySizedBox(
        heightFactor: 0.8,
        child: FutureBuilder(
          future: fetchData(),
          builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
              ? pi.ProgressIndicator()
              : Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            width: 90,
                            child:
                                Image.asset('assets/images/ic_grey_line.png')),
                      ),
                    ),
                    Positioned.fill(
                      top: 20,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 150),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: image != null
                              ? Image.memory(image, fit: BoxFit.cover)
                              : Icon(Icons.image),
                        ),
                      ),
                    ),
                    Positioned.fill(
                      top: 180,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(34.0, 0, 34.0, 8),
                        child: Container(
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 6.0,
                              ),
                            ],
                            borderRadius: new BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    16.0, 16.0, 16.0, 8.0),
                                child: Text(
                                  result.name ?? '',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    16.0, 0, 16.0, 8.0),
                                child: Text(
                                  details.result.formattedAddress ?? '',
                                  maxLines: 2,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              details.result.website != null
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16.0),
                                      child: GestureDetector(
                                        child: Text(
                                          "Visit website",
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: colorDarkRed,
                                          ),
                                        ),
                                        onTap: () async {
                                          await _launchURL(
                                              details.result.website);
                                        },
                                      ),
                                    )
                                  : Container(),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    16.0, 20, 16.0, 12.0),
                                child: Row(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        _showReviews(context, result,
                                            details.result.reviews);
                                      },
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          RatingBarIndicator(
                                            rating: result?.rating ?? 0,
                                            itemBuilder: (context, index) =>
                                                Icon(
                                              Icons.star,
                                              color: colorGold,
                                            ),
                                            itemCount: 5,
                                            itemSize: 18.0,
                                            direction: Axis.horizontal,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 4),
                                            child: Text(
                                              '$reviewsSize reviews',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: colorDarkRed,
                                                decoration:
                                                    TextDecoration.underline,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    profile.isMapLeaveReviewAndAssignScore()
                                        ? Spacer()
                                        : Container(),
                                    profile.isMapLeaveReviewAndAssignScore()
                                        ? RaisedButton(
                                            child: Text(
                                              'Leave review',
                                              style: mainButtonTextStyle,
                                            ),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              Navigator.pushNamed(context,
                                                  MapReviewNewScreen.routeName,
                                                  arguments: result.placeId);
                                            },
                                          )
                                        : ConsentMessage.mapNoLeaveReview(),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  void _showReviews(
      BuildContext context, SearchResult result, List<Review> reviews) {
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;
    final app = Provider.of<AppProvider>(context, listen: false);
    app.usageRepo.ownReviewIsShown();

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      backgroundColor: Colors.white,
      builder: (BuildContext context) => FractionallySizedBox(
        heightFactor: 0.8,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                padding: const EdgeInsets.all(8.0),
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(40, 12, 40, 16),
                child: Text(
                  result.name,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 60.0, 8.0, 8.0),
              child: FutureBuilder(
                future: Provider.of<ReviewListProvider>(context, listen: false)
                    .fetchReviews(result.placeId, reviews ?? [], context),
                builder: (ctx, snp) => snp.connectionState ==
                        ConnectionState.waiting
                    ? Center(child: CircularProgressIndicator())
                    : Consumer<ReviewListProvider>(
                        child: Center(child: Text('No reviews')),
                        builder: (ctx, reviews, ch) => reviews.items.length <= 0
                            ? ch
                            : ListView.builder(
                                itemCount: profile.isMapShowReview()
                                    ? reviews.items.length
                                    : reviews.items.length + 1,
                                itemBuilder: (ctx, i) {
                                  var index =
                                      !profile.isMapShowReview() ? i - 1 : i;

                                  if (!profile.isMapShowReview() && i == 0) {
                                    return ConsentMessage.mapNoReview();
                                  }

                                  return Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(16, 8, 16, 8),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.grey[400],
                                                blurRadius: 6.0,
                                                offset: Offset(6, 0)),
                                          ],
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ListTile(
                                            leading: reviews.items[index].image
                                                    .path.isNotEmpty
                                                ? CircleAvatar(
                                                    radius: 28,
                                                    backgroundImage: FileImage(
                                                        reviews.items[index]
                                                            .image),
                                                  )
                                                : reviews.items[index]
                                                            .imageUrl !=
                                                        null
                                                    ? Image.network(reviews
                                                        .items[index].imageUrl)
                                                    : Icon(
                                                        Icons.account_circle,
                                                        size: 60.0,
                                                      ),
                                            title: Text(
                                              reviews.items[index]
                                                  .getAuthorName()
                                                  .toUpperCase(),
                                              style: TextStyle(
                                                  color: colorDarkRed,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            subtitle: Padding(
                                              padding:
                                                  const EdgeInsets.only(top: 8),
                                              child: RatingBarIndicator(
                                                rating: reviews
                                                    .items[index].score
                                                    .toDouble(),
                                                itemBuilder: (context, index) =>
                                                    Icon(
                                                  Icons.star,
                                                  color: colorGold,
                                                ),
                                                itemCount: 5,
                                                itemSize: 20.0,
                                                direction: Axis.horizontal,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                16, 2, 16, 4),
                                            child: Divider(
                                              color: Colors.grey[100],
                                              thickness: 1,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                16, 4, 16, 16),
                                            child: Text(
                                              reviews.items[index].reviewText ??
                                                  'Empty',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

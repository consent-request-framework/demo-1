import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/common/widgets/counter.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MapReviewStatisticScreen extends StatelessWidget {
  static const routeName = '/review-statistic';

  @override
  Widget build(BuildContext ctx) {
    final review = Provider.of<ReviewListProvider>(ctx, listen: false);
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Activity Statistics'),
        actions: <Widget>[Avatar()],
      ),
      body: profile.isMapShowStatistic() ? Center(
        child: Column(
          children: <Widget>[
            ProgressCounter(
              title: 'Total number of your reviews:',
              value: review.getTotalReviews(),
            ),
            ProgressCounter(
              title: 'Total amount of points for your reviews:',
              value: review.totalScores,
            ),
          ],
        ),
      ) : ConsentMessage.mapNoConsentForStatistic(ctx),
    );
  }
}

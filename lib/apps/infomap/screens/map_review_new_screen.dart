import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/models/review.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

class MapReviewNewScreen extends StatefulWidget {
  static const routeName = '/review-new';

  @override
  _MapReviewNewState createState() => _MapReviewNewState();
}

class _MapReviewNewState extends State<MapReviewNewScreen> {
  final _form = GlobalKey<FormState>();
  String firstName, lastName, reviewText;
  int _rating = 1;
  final _focusSurname = FocusNode();
  final _focusReview = FocusNode();

  @override
  Widget build(BuildContext context) {
    final String _placeId = ModalRoute.of(context).settings.arguments;
    var profileProvider = Provider.of<ProfileProvider>(context, listen: false);

    Future<void> _saveForm() async {
      if (!_form.currentState.validate()) {
        return;
      }

      _form.currentState.save();

      await Provider.of<ReviewListProvider>(context, listen: false).addReview(
        UserReview(
          placeId: _placeId,
          firstName: firstName,
          lastName: lastName,
          reviewText: reviewText,
          score: _rating,
          image: profileProvider.profile.avatar,
        ),
      );

      await Provider.of<ProfileProvider>(context, listen: false)
          .updateTotalScore();

      Navigator.of(context).pop();
    }

    String _validateMandatory(value) {
      return value.isEmpty ? 'This field is required' : null;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Review'),
        actions: <Widget>[Avatar()],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Form(
                      key: _form,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 16.0,
                          ),
                          TextFormField(
                            decoration:
                                InputDecoration(labelText: 'First Name'),
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (v) {
                              FocusScope.of(context)
                                  .requestFocus(_focusSurname);
                            },
                            onSaved: (value) {
                              firstName = value;
                            },
                            validator: _validateMandatory,
                            initialValue: profileProvider.profile?.firstName,
                          ),
                          SizedBox(height: 16.0),
                          TextFormField(
                            decoration: InputDecoration(labelText: 'Last Name'),
                            textInputAction: TextInputAction.next,
                            focusNode: _focusSurname,
                            onFieldSubmitted: (v) {
                              FocusScope.of(context).requestFocus(_focusReview);
                            },
                            onSaved: (value) {
                              lastName = value;
                            },
                            validator: _validateMandatory,
                            initialValue: profileProvider.profile?.lastName,
                          ),
                          SizedBox(height: 16.0),
                          Text(
                            "Set rate",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 8.0),
                          RatingBar(
                            initialRating: 1,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: false,
                            itemCount: 5,
                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: colorGold,
                            ),
                            onRatingUpdate: (rating) {
                              _rating = rating.toInt();
                            },
                          ),
                          SizedBox(height: 16.0),
                          TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Leave your review'),
                            keyboardType: TextInputType.multiline,
                            maxLines: 5,
                            maxLength: 500,
                            textAlign: TextAlign.start,
                            focusNode: _focusReview,
                            textInputAction: TextInputAction.done,
                            onSaved: (value) {
                              reviewText = value;
                            },
                            validator: _validateMandatory,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(5),
            child: RaisedButton(
              child: Text('Leave review', style: mainButtonTextStyle),
              onPressed: _saveForm,
            ),
          ),
        ],
      ),
    );
  }
}

import 'dart:io';

import 'package:demo1/database/database.dart';
import 'package:demo1/enums/filter_group_enum.dart';
import 'package:demo1/models/review.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:provider/provider.dart';

class ReviewListProvider with ChangeNotifier {
  final UsagePurposeRepository usageRepo;
  List<UserReview> _items = [];

  ReviewListProvider(this.usageRepo);

  List<UserReview> get items {
    return [..._items];
  }

  int _totalReviews = 0;

  int getTotalReviews() {
    usageRepo.statisticIsViewed();
    return _totalReviews;
  }
  int _totalScores = 0;

  int get totalScores => _totalScores;

  bool _parksSelected = false;
  bool _restSelected = false;
  bool _bikesSelected = false;
  bool _poiSelected = false;

  bool get parksSelected => _parksSelected;

  set parksSelected(bool value) {
    _parksSelected = value;
    notifyListeners();
  }

  bool get restSelected => _restSelected;

  set restSelected(bool value) {
    _restSelected = value;
    notifyListeners();
  }

  bool get bikesSelected => _bikesSelected;

  set bikesSelected(bool value) {
    _bikesSelected = value;
    notifyListeners();
  }

  bool get poiSelected => _poiSelected;

  set poiSelected(bool value) {
    _poiSelected = value;
    notifyListeners();
  }

  Future<void> initStatistic() async {
    final reviewDb = await DB.query(UserReview.table);
    List<UserReview> reviews =
        reviewDb.map<UserReview>((x) => UserReview.fromMap(x)).toList();

    _totalReviews = reviews.length;
    _totalScores = reviews.length * 12;

    notifyListeners();
  }

  initPreselection(Set<FilterEnum> selectedMapFilters) {
    _parksSelected = false;
    _restSelected = false;
    _poiSelected = false;
    _bikesSelected = false;

    for (FilterEnum filter in selectedMapFilters) {
      if (filter == FilterEnum.park) {
        _parksSelected = true;
      } else if (filter == FilterEnum.restaurant) {
        _restSelected = true;
      } else if (filter == FilterEnum.poi) {
        _poiSelected = true;
      } else if (filter == FilterEnum.bike) {
        _bikesSelected = true;
      }
    }

    notifyListeners();
  }

  Future<void> addReview(UserReview review) async {
    UserReview item = UserReview(
      score: review.score,
      firstName: review.firstName,
      lastName: review.lastName,
      placeId: review.placeId,
      image: review.image,
      reviewText: review.reviewText,
      timestamp: DateTime.now(),
    );
    item.id = await DB.insert(UserReview.table, item);
    _items.insert(0, item);

    await usageRepo.reviewIsAdded();
    await initStatistic();

    notifyListeners();
  }

  Future<void> fetchReviews(String placeId, List<Review> reviews, context) async {
    final dataList = await DB
        .query(UserReview.table, where: 'placeId = ?', whereArgs: [placeId]);
    final placesReviews = reviews
        .map<UserReview>((item) => UserReview.fromPlaceReview(item))
        .toList();
    var userReviews =
        dataList.map<UserReview>((item) => UserReview.fromMap(item)).toList();
    final profile =
        Provider.of<ProfileProvider>(context, listen: false).profile;

    if (!profile.isPrefillAvatar()) {
      for (var userReview in userReviews) {
        userReview.image = File('');
      }
    }

    if (!profile.isMapShowReview()) {
      userReviews = [];
    }
    _items = userReviews + placesReviews;

    notifyListeners();
  }

  Future<int> getUserReviewsSize(String placeId) async {
    final dataList = await DB
        .query(UserReview.table, where: 'placeId = ?', whereArgs: [placeId]);
    return dataList.length;
  }
}

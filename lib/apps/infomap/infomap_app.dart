import 'package:demo1/apps/infomap/screens/map_review_new_screen.dart';
import 'package:demo1/apps/infomap/screens/map_review_statistic_screen.dart';
import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/map_screen.dart';

class InfomapApp extends StatelessWidget {
  const InfomapApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('InfomapApp');
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case MapScreen.routeName:
                return MapScreen();
              case MapReviewNewScreen.routeName:
                return MapReviewNewScreen();
              case MapReviewStatisticScreen.routeName:
                return MapReviewStatisticScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}

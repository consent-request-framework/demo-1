import 'package:demo1/database/database.dart';
import 'package:demo1/enums/shop_order_status_enum.dart';
import 'package:demo1/models/shop_item.dart';
import 'package:demo1/models/shop_order.dart';
import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/material.dart';

class ShopOrderListProvider with ChangeNotifier {
  final UsagePurposeRepository usageRepo;
  List<ShopOrder> _items = [];

  ShopOrderListProvider(this.usageRepo);

  List<ShopOrder> get items {
    return [..._items];
  }

  Future<void> fetchShopOrders() async {
    final dataList = await DB.query(ShopOrder.table, orderBy: 'status ASC');
    _items = dataList.map<ShopOrder>((x) => ShopOrder.fromMap(x)).toList();
    notifyListeners();
  }

  Future<void> addShopOrder(ShopOrder shopOrder, ShopItem shopItem) async {
    ShopOrder item = shopOrder.copyWith(
      shopItemName: shopItem.name,
      shopItemPrice: shopItem.price,
      shopItemAddress: shopItem.address,
      shopItemImageUri: shopItem.imageUri,
      status: ShopOrderStatusEnum.active,
      timestamp: DateTime.now(),
    );
    item.id = await DB.insert(ShopOrder.table, item);
    _items.insert(0, item);

    await usageRepo.ticketIsAdded();

    notifyListeners();
  }

  Future<void> useShopOrder(ShopOrder shopOrder) async {
    ShopOrder item = shopOrder.copyWith(
      status: ShopOrderStatusEnum.used,
    );

    await DB.update(ShopOrder.table, item);
    final index = _items.indexWhere((x) => x.id == shopOrder.id);
    _items[index] = item.copyWith();
    notifyListeners();
  }

  ShopOrder findById(int id) {
    return _items.firstWhere((report) => report.id == id);
  }

  bool isBought(String imageUri) {
    return _items
            .where((report) => report.shopItemImageUri == imageUri)
            .toList()
            .length >= 1;
  }

  Future<int> ticketNumber() async {
    final dataList = await DB.query(ShopOrder.table);
    final items = dataList.map<ShopOrder>((x) => ShopOrder.fromMap(x)).toList();
    return items.length;
  }
}

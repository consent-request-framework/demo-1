import 'dart:convert';

import 'package:demo1/models/shop_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ShopItemListProvider with ChangeNotifier {
  List<ShopItem> _items = [];

  List<ShopItem> get items {
    return [..._items];
  }

  Future<void> fetch() async {
    final data =
        await rootBundle.loadString('assets/jsons/available_tickets.json');
    final jsonResult = json.decode(data);

    _items = [];
    for (final item in jsonResult) {
      _items.add(ShopItem.fromMap(item));
    }
    _items.sort((a, b) => a.id.compareTo(b.id));
//    _items.sort((a, b) => a.name.compareTo(b.name));

    notifyListeners();
  }

  ShopItem findById(int id) {
    return _items.firstWhere((report) => report.id == id);
  }
}

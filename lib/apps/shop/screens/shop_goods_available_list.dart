import 'package:demo1/apps/shop/providers/shop_item_list_provider.dart';
import 'package:demo1/apps/shop/providers/shop_order_list_provider.dart';
import 'package:demo1/apps/shop/widgets/shop_item_card.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'shop_goods_purchase_item.dart';
import 'shop_goods_purchased_list.dart';

class ShopGoodsAvailableList extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Available Tickets'),
        actions: <Widget>[Avatar()],
        leading: IconButton(
          icon: Consumer<ShopOrderListProvider>(builder: (ctx, shopItems, ch) {
            return Image.asset(
              'assets/images/ic_tickets.png',
              color: shopItems.items.length > 0 ? colorDarkRed : Colors.black,
            );
          }),
          onPressed: () {
            Navigator.pushNamed(ctx, ShopGoodsPurchasedList.routeName);
          },
        ),
      ),
      body: FutureBuilder(
        future: Provider.of<ShopItemListProvider>(ctx, listen: false).fetch(),
        builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
            ? Center(child: CircularProgressIndicator())
            : Consumer<ShopItemListProvider>(
                child: Center(child: Text('No Items')),
                builder: (ctx, shopItems, ch) => shopItems.items.length <= 0
                    ? ch
                    : ListView.builder(
                        itemCount: shopItems.items.length,
                        itemBuilder: (ctx, i) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ShopItemCard(
                            shopItem: ShopItemCardModel(
                              name: shopItems.items[i].name,
                              address: shopItems.items[i].address,
                              imageUri: shopItems.items[i].imageUri,
                              info: '${shopItems.items[i].price} points',
                            ),
                            onTap: () {
                              Navigator.pushNamed(
                                  ctx, ShopGoodsPurchaseItem.routeName,
                                  arguments: shopItems.items[i].id);
                            },
                          ),
                        ),
                      ),
              ),
      ),
    );
  }
}

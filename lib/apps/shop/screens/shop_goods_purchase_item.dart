import 'package:demo1/apps/shop/providers/shop_item_list_provider.dart';
import 'package:demo1/apps/shop/providers/shop_order_list_provider.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/common/helpers/modal_dialog.dart';
import 'package:demo1/models/address.dart';
import 'package:demo1/models/profile.dart';
import 'package:demo1/models/shop_item.dart';
import 'package:demo1/models/shop_order.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShopGoodsPurchaseItem extends StatefulWidget {
  static const routeName = 'shop-goods-purchase-item';

  @override
  _ShopGoodsPurchaseItemState createState() => _ShopGoodsPurchaseItemState();
}

class _ShopGoodsPurchaseItemState extends State<ShopGoodsPurchaseItem> {
  final _form = GlobalKey<FormState>();
  final _firstNameFocusNode = FocusNode();
  final _lastNameFocusNode = FocusNode();
  final _streetFocusNode = FocusNode();
  final _zipCodeFocusNode = FocusNode();
  final _cityCodeFocusNode = FocusNode();

  ShopItem _shopItem;
  ShopOrder _model = ShopOrder(address: Address());
  bool _isInit = false;
  bool _canBuy = true;

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final shopItemId = ModalRoute.of(context).settings.arguments as int;
      _shopItem = Provider.of<ShopItemListProvider>(context, listen: false)
          .findById(shopItemId);
      final score =
          Provider.of<ProfileProvider>(context, listen: false).profile.age;
      _canBuy = _shopItem.price <= score;
      _isInit = true;
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _firstNameFocusNode.dispose();
    _lastNameFocusNode.dispose();
    _streetFocusNode.dispose();
    _zipCodeFocusNode.dispose();
    _cityCodeFocusNode.dispose();
    super.dispose();
  }

  Future<void> _saveForm() async {
    if (!_form.currentState.validate()) {
      return;
    }

    _form.currentState.save();

    var shop = Provider.of<ShopOrderListProvider>(context, listen: false);
    var exists = shop.isBought(_shopItem.imageUri);
    var areYouSure = await ModalDialog.showAreYouSure(
      context,
      body: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 16.0,
            color: Colors.black,
          ),
          children: <TextSpan>[
            exists ? TextSpan(text: 'You already have unused ticket(s) to ${_shopItem.name}. \n\n') : TextSpan(text:''),
            TextSpan(text: 'Are you sure you want to exchange '),
            TextSpan(
              text: '${_shopItem.price} pts',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            TextSpan(text: ' for the '),
            TextSpan(
              text: _shopItem.name,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            TextSpan(text: ' ticket?')
          ],
        ),
      ),
      yesBtn: 'Yes, I\'m sure',
      noBtn: 'Cancel',
    );

    if (!areYouSure) {
      return;
    }

    await shop.addShopOrder(_model, _shopItem);

    await Provider.of<ProfileProvider>(context, listen: false)
        .updateTotalScore();

    Scaffold.of(context).showSnackBar(_snackBar);
    Navigator.popUntil(context, ModalRoute.withName('/'));
  }

  final _snackBar = SnackBar(
    content: Text('You have exchanged your scores.'),
    duration: Duration(milliseconds: 2000),
  );

  TextFormField _createField({
    String label,
    TextInputAction textInputAction,
    FocusNode currentFocusNode,
    FocusNode nextFocusNode,
    Function onSave,
    TextInputType keyboardType,
    initialValue,
  }) {
    return TextFormField(
      decoration: InputDecoration(labelText: label),
      initialValue: initialValue?.toString(),
      focusNode: currentFocusNode,
      keyboardType: keyboardType ?? TextInputType.text,
      textInputAction:
          textInputAction == null ? TextInputAction.next : textInputAction,
      validator: (value) {
        return value.isEmpty ? 'This field is required' : null;
      },
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(nextFocusNode);
      },
      onSaved: (value) {
        onSave(value);
      },
    );
  }

  @override
  Widget build(BuildContext ctx) {
    Profile profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;

    String getStreet() {
      if (profile.isPrefillAddress()) {
        return profile.address?.street ?? null;
      }
      return null;
    }

    int getZipCode() {
      if (profile.isPrefillAddress()) {
        return profile.address?.zipCode ?? null;
      }
      return null;
    }

    String getCity() {
      if (profile.isPrefillAddress()) {
        return profile.address?.city ?? null;
      }
      return null;
    }

    String getFirstName() {
      if (profile.isPrefillFirstLastName()) {
        return profile.firstName ?? null;
      }
      return null;
    }

    String getLastName() {
      if (profile.isPrefillFirstLastName()) {
        return profile.lastName ?? null;
      }
      return null;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Ticket Purchase'),
        centerTitle: true,
        actions: <Widget>[Avatar()],
      ),
      body: profile.isPurchaseAvailable() ? Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                  left: 40.0,
                  right: 40.0,
                  top: 20,
                ),
                child: Form(
                  key: _form,
                  child: Column(
                    children: <Widget>[
                      Text(
                        _shopItem.name,
                        style: Theme.of(context).textTheme.headline6.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 5,
                              blurRadius: 15,
                            ),
                          ],
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Image(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              _shopItem.imageUri,
                            ),
                            height: 150.0,
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text(
                              'We\'ll send the ticket to your address:',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                    fontSize: 18,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      _createField(
                        label: 'First Name',
                        currentFocusNode: _firstNameFocusNode,
                        nextFocusNode: _lastNameFocusNode,
                        onSave: (value) {
                          _model = _model.copyWith(firstName: value);
                        },
                        initialValue: getFirstName(),
                      ),
                      SizedBox(height: 20),
                      _createField(
                        label: 'Last Name',
                        currentFocusNode: _lastNameFocusNode,
                        nextFocusNode: _streetFocusNode,
                        onSave: (value) {
                          _model = _model.copyWith(lastName: value);
                        },
                        initialValue: getLastName(),
                      ),
                      SizedBox(height: 20),
                      _createField(
                        label: 'Street, House #',
                        currentFocusNode: _streetFocusNode,
                        nextFocusNode: _zipCodeFocusNode,
                        onSave: (value) {
                          _model = _model.copyWith(
                            address: _model.address.copyWith(street: value),
                          );
                        },
                        initialValue: getStreet(),
                      ),
                      SizedBox(height: 20),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: _createField(
                              label: 'Zip Code',
                              currentFocusNode: _zipCodeFocusNode,
                              nextFocusNode: _cityCodeFocusNode,
                              keyboardType: TextInputType.number,
                              onSave: (value) {
                                _model = _model.copyWith(
                                  address: _model.address
                                      .copyWith(zipCode: int.parse(value)),
                                );
                              },
                              initialValue: getZipCode(),
                            ),
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: _createField(
                              label: 'City',
                              textInputAction: TextInputAction.done,
                              currentFocusNode: _cityCodeFocusNode,
                              onSave: (value) {
                                _model = _model.copyWith(
                                  address: _model.address.copyWith(city: value),
                                );
                              },
                              initialValue: getCity(),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(color: Colors.grey.withOpacity(0.7), blurRadius: 3.0)
              ],
            ),
            child: _canBuy
                ? RaisedButton(
                    child: Text(
                      'Get for ${_shopItem.price} pts',
                      style: TextStyle(color: colorDarkRed, fontSize: 18),
                    ),
                    onPressed: _saveForm,
                  )
                : Text(
                    'Not enough scores to buy',
                    style: TextStyle(color: colorDarkRed, fontSize: 18),
                  ),
          ),
        ],
      ) : ConsentMessage.noPurchase(context),
    );
  }
}

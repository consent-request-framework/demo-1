import 'package:demo1/apps/shop/providers/shop_order_list_provider.dart';
import 'package:demo1/apps/shop/widgets/shop_item_card.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/enums/shop_order_status_enum.dart';
import 'package:demo1/models/shop_order.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShopGoodsPurchasedList extends StatelessWidget {
  static const routeName = 'shop-goods-purchased-list';

  @override
  Widget build(BuildContext context) {
    String _getStatus(ShopOrder order) {
      if (order.status == ShopOrderStatusEnum.expired) {
        return 'Expired';
      }
      if (order.status == ShopOrderStatusEnum.used) {
        return 'Used';
      }
      var days = order.timestamp
          .add(Duration(days: 366))
          .difference(DateTime.now())
          .inDays;
      var suffix = days > 1 ? 's' : '';
      return '$days day$suffix left';
    }

    bool _isActive(ShopOrder item) {
      return item.status == ShopOrderStatusEnum.active;
    }

    void _showBarcode(ShopOrder item) {
      showModalBottomSheet<void>(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(15.0),
                topRight: const Radius.circular(15.0),
              ),
            ),
            height: MediaQuery.of(context).size.height * 0.70,
            child: Column(
              children: <Widget>[
                SizedBox(height: 2),
                Container(
                  width: 90,
                  child: Image.asset('assets/images/ic_grey_line.png'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
//                    IconButton(
//                      icon: Icon(Icons.done),
//                      onPressed: () async => {
//                        await Provider.of<ShopOrderListProvider>(context,
//                                listen: false)
//                            .useShopOrder(item),
//                        Navigator.pop(context),
//                      },
//                    ),
                    SizedBox(width: 50.0),
                    Text(
                      item.shopItemName,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Spacer(),
                    ImageIcon(
                      AssetImage(
                        'assets/images/ic_location.png',
                      ),
                      size: 25,
                      color: colorDarkRed,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(item.shopItemAddress),
                    Spacer()
                  ],
                ),
                Spacer(),
                Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    side: BorderSide(color: Colors.black26),
                  ),
                  child: Image(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/qr_code.png'),
                    height: 250,
                  ),
                ),
                Spacer(),
                Text('Show this QR code at the ticket counter'),
                SizedBox(height: 15)
              ],
            ),
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Tickets'),
        centerTitle: true,
        actions: <Widget>[Avatar()],
      ),
      body: FutureBuilder(
        future: Provider.of<ShopOrderListProvider>(context, listen: false)
            .fetchShopOrders(),
        builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
            ? Center(child: CircularProgressIndicator())
            : Consumer<ShopOrderListProvider>(
                child: Center(child: Text('No Tickets')),
                builder: (ctx, orders, ch) => orders.items.length <= 0
                    ? ch
                    : ListView.builder(
                        itemCount: orders.items.length,
                        itemBuilder: (ctx, i) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ShopItemCard(
                            shopItem: ShopItemCardModel(
                              name: orders.items[i].shopItemName,
                              address: orders.items[i].shopItemAddress ?? '',
                              imageUri: orders.items[i].shopItemImageUri,
                              info: _getStatus(orders.items[i]),
                            ),
                            onTap: () {
                              _showBarcode(orders.items[i]);
                            },
                            enabled: _isActive(orders.items[i]),
                          ),
                        ),
                      ),
              ),
      ),
    );
  }
}

import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/shop_goods_available_list.dart';
import 'screens/shop_goods_purchase_item.dart';
import 'screens/shop_goods_purchased_list.dart';

class ShopApp extends StatelessWidget {
  const ShopApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('ShopApp');
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case ShopGoodsAvailableList.routeName:
                return ShopGoodsAvailableList();
              case ShopGoodsPurchaseItem.routeName:
                return ShopGoodsPurchaseItem();
              case ShopGoodsPurchasedList.routeName:
                return ShopGoodsPurchasedList();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}

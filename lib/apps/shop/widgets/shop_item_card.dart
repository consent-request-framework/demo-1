import 'package:demo1/common/constants.dart';
import 'package:flutter/material.dart';

class ShopItemCard extends StatelessWidget {
  final ShopItemCardModel shopItem;
  final Function onTap;
  final bool enabled;

  const ShopItemCard({
    @required this.shopItem,
    this.enabled = true,
    this.onTap,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: !enabled,
      child: Card(
        color: enabled ? Colors.white : Colors.black12.withOpacity(0.05),
        elevation: enabled ? 3 : 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
          side: enabled
              ? BorderSide.none
              : BorderSide(color: Colors.black26.withOpacity(0.07)),
        ),
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: shopItem.imageUri != null
                      ? Image(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            shopItem.imageUri,
                          ),
                          width: 95.0,
                          height: 95.0,
                        )
                      : null,
                ),
                SizedBox(width: 15),
                Flexible(
                  child: Container(
                    height: 95.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          shopItem.name,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Spacer(),
                        Row(
                          children: <Widget>[
                            ImageIcon(
                              AssetImage(
                                'assets/images/ic_location.png',
                              ),
                              size: 25,
                              color: colorDarkRed,
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Flexible(
                              child: Text(shopItem.address),
                            ),
                          ],
                        ),
                        Spacer(),
                        Text(
                          shopItem.info,
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ShopItemCardModel {
  final String name;
  final String imageUri;
  final String address;
  final String info;

  ShopItemCardModel({
    this.name,
    this.imageUri,
    this.address,
    this.info,
  });
}

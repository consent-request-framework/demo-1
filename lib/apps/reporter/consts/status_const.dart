import 'package:demo1/common/constants.dart';
import 'package:demo1/enums/report_status_enum.dart';
import 'package:flutter/material.dart';

class StatusConst {
  static Map<ReportStatusEnum, IconData> statuses = {
    ReportStatusEnum.submitted: Icons.receipt,
    ReportStatusEnum.inProgress: Icons.departure_board,
    ReportStatusEnum.rejected: Icons.remove_circle_outline,
    ReportStatusEnum.done: Icons.done_outline,
  };

  static Map<ReportStatusEnum, Color> statusColors = {
    ReportStatusEnum.submitted: colorGold,
    ReportStatusEnum.inProgress: colorGold,
    ReportStatusEnum.rejected: colorDarkRed,
    ReportStatusEnum.done: colorGreen,
  };

}

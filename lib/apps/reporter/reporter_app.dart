import 'package:demo1/apps/reporter/screens/reporter_problem_new_screen.dart';
import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/reporter_problem_list_screen.dart';
import 'screens/reporter_problem_status_screen.dart';
import 'screens/reporter_statistic_screen.dart';

class ReporterApp extends StatelessWidget {
  const ReporterApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('ReporterApp');
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case ReporterStatisticScreen.routeName:
                return ReporterStatisticScreen();
              case ReporterProblemListScreen.routeName:
                return ReporterProblemListScreen();
              case ReporterProblemNewScreen.routeName:
                return ReporterProblemNewScreen();
              case ReporterProblemStatusScreen.routeName:
                return ReporterProblemStatusScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}

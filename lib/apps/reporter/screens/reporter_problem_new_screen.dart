import 'package:demo1/apps/reporter/providers/report_list_provider.dart';
import 'package:demo1/apps/reporter/widgets/image_input.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/modal_dialog.dart';
import 'package:demo1/helpers/google_helper.dart';
import 'package:demo1/models/location.dart' as modelLocation;
import 'package:demo1/common/widgets/progress_indicator.dart' as pi;
import 'package:demo1/models/report.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class ReporterProblemNewScreen extends StatefulWidget {
  static const routeName = 'report-new';

  @override
  _ReporterProblemNewScreenState createState() =>
      _ReporterProblemNewScreenState();
}

class _ReporterProblemNewScreenState extends State<ReporterProblemNewScreen> {
  final _descriptionFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _report = Report();
  var _coordinatesController = TextEditingController();
  modelLocation.Location _location;

  @override
  void dispose() {
    _descriptionFocusNode.dispose();
    super.dispose();
  }

  Future<void> getLocation() async {
    final location = await GoogleHelper.getLocation(context);

    _location = modelLocation.Location(
      latitude: location.latitude,
      longitude: location.longitude,
    );

    if (location.latitude != 48.2082 && location.longitude != 16.3738) {
      var newCoordinatesValue =
          '${location.latitude.toStringAsFixed(4)}, ${location.longitude.toStringAsFixed(4)}';
      _coordinatesController.value = TextEditingValue(
        text: newCoordinatesValue,
        selection: TextSelection.fromPosition(
          TextPosition(offset: newCoordinatesValue.length),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _saveForm() async {
      if (_report.image == null) {
        await ModalDialog.showWarning(
          context,
          body: Text(
            'Please make a photo!',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        );
        return;
      }

      var value = _coordinatesController.value.text;
      if (value == null || value == '') {
        await ModalDialog.showWarning(
          context,
          body: Text(
            'Please select a location!',
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        );
        return;
      }

      if (!_form.currentState.validate()) {
        return;
      }

      _form.currentState.save();

      var areYouSure = await ModalDialog.showAreYouSure(
        context,
        body: Text(
          'Would you like to send your report?',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 16,
          ),
        ),
        yesBtn: 'Send Report',
        noBtn: 'Cancel',
      );

      if (!areYouSure) {
        return;
      }

      await Provider.of<ReportListProvider>(context, listen: false)
          .addReport(_report);
      await Provider.of<ProfileProvider>(context, listen: false)
          .updateTotalScore();

      Navigator.of(context).pop();
    }

    String _validateMandatory(value) {
      return value.isEmpty ? 'This field is required' : null;
    }

    String _validateLocation(value) {
      return value == null ? 'Location is required' : null;
    }

    void _showMap() {
      showModalBottomSheet(
        context: context,
        enableDrag: false,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          Marker _newMarker(LatLng point) {
            return Marker(
              markerId: MarkerId(point.toString()),
              position: point,
              icon: BitmapDescriptor.defaultMarkerWithHue(
                  BitmapDescriptor.hueRed),
              infoWindow: InfoWindow(
                title: 'Selected Position',
              ),
            );
          }

          final pos = LatLng(_location.latitude, _location.longitude);
          Set<Marker> _markers = {_newMarker(pos)};

          final profile =
              Provider.of<ProfileProvider>(context, listen: false).profile;

          return StatefulBuilder(builder: (BuildContext context, setState) {
            void _setMarker(LatLng point) {
              Set<Marker> markers = Set();
              var marker = _newMarker(point);
              markers.add(marker);
              setState(() {
                _markers = markers;
              });
            }

            return Container(
              height: MediaQuery.of(context).size.height + 100,
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                    ),
                    child: GoogleMap(
                      markers: _markers,
                      mapType: MapType.normal,
                      zoomControlsEnabled: true,
                      zoomGesturesEnabled: true,
                      mapToolbarEnabled: false,
                      myLocationEnabled: profile.isMapShowCurrentLocation(),
                      myLocationButtonEnabled: true,
                      initialCameraPosition: CameraPosition(
                        target: pos,
                        zoom: 15.0000,
                      ),
                      onTap: _setMarker,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: RaisedButton(
                        color: Colors.white,
                        onPressed: () {
                          setState(() {
                            _location = modelLocation.Location(
                                latitude: _markers.first.position.latitude,
                                longitude: _markers.first.position.longitude);
                            final newValue =
                                '${_markers.first.position.latitude.toStringAsFixed(4)}, ${_markers.first.position.longitude.toStringAsFixed(4)}';
                            _coordinatesController.value = TextEditingValue(
                              text: newValue,
                              selection: TextSelection.fromPosition(
                                TextPosition(offset: newValue.length),
                              ),
                            );
                          });
                          Navigator.pop(context);
                        },
                        child: Text('OK', style: mainButtonTextStyle),
                      ),
                    ),
                  ),
                ],
              ),
            );
          });
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Report a problem'),
        actions: <Widget>[Avatar()],
      ),
      body: FutureBuilder(
        future: getLocation(),
        builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
            ? Center(child: pi.ProgressIndicator())
            : Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Form(
                        key: _form,
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 12),
                            ImageInput(
                              (file) =>
                                  {_report = _report.copyWith(image: file)},
                            ),
                            SizedBox(height: 10),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 8, 24, 8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 4, right: 2),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          'Location',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14),
                                        ),
                                        Spacer(),
                                        Image.asset(
                                          'assets/images/ic_marker.png',
                                          width: 14,
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        InkWell(
                                          onTap: _showMap,
                                          child: Text(
                                            'Find on map',
                                            textAlign: TextAlign.end,
                                            style: TextStyle(
                                                color: colorDarkRed,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  TextFormField(
                                    decoration: InputDecoration(
                                        labelText: 'Coordinates'),
                                    textInputAction: TextInputAction.next,
                                    readOnly: true,
                                    controller: _coordinatesController,
                                    validator: _validateLocation,
                                    onSaved: (value) {
                                      _report =
                                          _report.copyWith(location: _location);
                                    },
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 8, 24, 8),
                              child: TextFormField(
                                decoration:
                                    InputDecoration(labelText: 'Description'),
                                keyboardType: TextInputType.multiline,
                                focusNode: _descriptionFocusNode,
                                maxLines: 5,
                                maxLength: 500,
                                textInputAction: TextInputAction.done,
                                validator: _validateMandatory,
                                onSaved: (value) {
                                  _report =
                                      _report.copyWith(description: value);
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(5),
                    child: RaisedButton(
                      child: Text('Save', style: mainButtonTextStyle),
                      onPressed: _saveForm,
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}

import 'package:demo1/apps/reporter/providers/report_list_provider.dart';
import 'package:demo1/apps/reporter/widgets/report_item_card.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'reporter_problem_new_screen.dart';
import 'reporter_problem_status_screen.dart';
import 'reporter_statistic_screen.dart';

class ReporterProblemListScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext ctx) {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    final app = Provider.of<AppProvider>(ctx, listen: false);
    if (profile.isReportListAvailable()) {
      app.usageRepo.listReportsIsShown();
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Problem Reporter'),
        actions: <Widget>[Avatar()],
        leading: IconButton(
          icon: ImageIcon(
            AssetImage("assets/images/ic_statistics.png"),
            color: Colors.black,
          ),
          onPressed: () => {
            Navigator.pushNamed(ctx, ReporterStatisticScreen.routeName),
          },
        ),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: profile.isReportCreationAvailable()
                ? Padding(
                    padding: const EdgeInsets.only(top: 15, bottom: 15),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(ctx)
                            .pushNamed(ReporterProblemNewScreen.routeName);
                      },
                      child: Text(
                        'Report a problem',
                        style: mainButtonTextStyle,
                      ),
                    ),
                  )
                : Container(),
          ),
          Expanded(
            child: profile.isReportListAvailable()
                ? FutureBuilder(
                    future: Provider.of<ReportListProvider>(ctx, listen: false)
                        .fetchReports(ctx),
                    builder: (ctx, snp) => snp.connectionState ==
                            ConnectionState.waiting
                        ? Center(child: CircularProgressIndicator())
                        : Consumer<ReportListProvider>(
                            child: Center(child: Text('No reports')),
                            builder: (ctx, reports, ch) => reports
                                        .items.length <=
                                    0
                                ? ch
                                : ListView.builder(
                                    itemCount: reports.items.length,
                                    itemBuilder: (ctx, i) => Padding(
                                      padding: const EdgeInsets.only(
                                        left: 8.0,
                                        right: 8.0,
                                        bottom: 16.0,
                                      ),
                                      child: ReportItemCard(
                                        reportItem: ReportItemCardModel(
                                          name: reports.items[i].description,
                                          address: reports.items[i].address,
                                          image: reports.items[i].image,
                                          status: reports.items[i].status,
                                        ),
                                        onTap: () {
                                          Navigator.pushNamed(
                                              ctx,
                                              ReporterProblemStatusScreen
                                                  .routeName,
                                              arguments: reports.items[i].id);
                                        },
                                      ),
                                    ),
                                  ),
                          ),
                  )
                : ConsentMessage.reportNoConsentForList(ctx),
          )
        ],
      ),
    );
  }
}

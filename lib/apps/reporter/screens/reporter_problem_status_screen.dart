import 'package:demo1/apps/reporter/consts/status_const.dart';
import 'package:demo1/apps/reporter/providers/report_list_provider.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/models/report.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReporterProblemStatusScreen extends StatelessWidget {
  static const routeName = 'report-status';

  @override
  Widget build(BuildContext ctx) {
    final id = ModalRoute.of(ctx).settings.arguments;
    final report =
        Provider.of<ReportListProvider>(ctx, listen: false).findById(id);
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    final app = Provider.of<AppProvider>(ctx, listen: false);
    if (profile.isReportStatusAvailable()) {
      app.usageRepo.reportStatusIsShown();
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Problem Details'),
        actions: <Widget>[Avatar()],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 15.0,
            ),
            Container(
              height: 250,
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 16),
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                    color: Colors.grey[200],
                    blurRadius: 3.0,
                    offset: Offset(3, 0)),
              ], borderRadius: BorderRadius.circular(20)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: Image.file(
                  report.image,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(40, 20, 40, 0),
              child: profile.isReportStatusAvailable()
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Status',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            Report.getStatusString(report.status),
                            style: TextStyle(
                                color: StatusConst.statusColors[report.status],
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    )
                  : ConsentMessage.reportNoStatusText(ctx),
            ),
            profile.isReportStatusAvailable()
                ? Padding(
                    padding: const EdgeInsets.fromLTRB(40, 10, 40, 0),
                    child: TextFormField(
                      decoration: InputDecoration(
                          labelText: 'Comments',
                          contentPadding: const EdgeInsets.all(16.0)),
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      initialValue: report.statusMessage,
                      enabled: false,
                    ),
                  )
                : Container(),
            Padding(
              padding: const EdgeInsets.fromLTRB(40, 24, 40, 12),
              child: TextFormField(
                decoration: InputDecoration(labelText: 'Coordinates'),
                initialValue:
                    '${report.location.latitude.toStringAsFixed(4)}, ${report.location.longitude.toStringAsFixed(4)}',
                enabled: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(40, 12, 40, 12),
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                maxLines: 5,
                decoration: InputDecoration(
                    labelText: 'Description',
                    contentPadding: const EdgeInsets.all(16.0)),
                initialValue: report.description ?? '',
                enabled: false,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:demo1/apps/reporter/providers/report_list_provider.dart';
import 'package:demo1/common/avatar.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/common/widgets/counter.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReporterStatisticScreen extends StatelessWidget {
  static const routeName = 'report-statistic';

  @override
  Widget build(BuildContext ctx) {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    final reportData = Provider.of<ReportListProvider>(ctx, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Activity Statistics'),
        centerTitle: true,
        actions: <Widget>[Avatar()],
      ),
      body: profile.isReportStatisticAvailable()
          ? Center(
              child: Column(
                children: <Widget>[
                  ProgressCounter(
                    title: 'Total number of your reported issues:',
                    value: reportData.getTotalReports(),
                  ),
                  ProgressCounter(
                    title: 'Total amount of points for reported issues:',
                    value: reportData.totalScores,
                  ),
                ],
              ),
            )
          : ConsentMessage.reportNoConsentForStatistic(ctx),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;

class ImageInput extends StatefulWidget {
  final Function onSelectImage;

  ImageInput(this.onSelectImage);

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File _storedImage;

  Future<void> _takePicture() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.getImage(
        source: ImageSource.camera,
        maxWidth: 600
    );
    if (imageFile == null) {
      return;
    }

    final file = File(imageFile.path);

    setState(() {
      _storedImage = file;
    });
    final appDir = await syspaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    final savedImage = await file.copy('${appDir.path}/$fileName');
    widget.onSelectImage(savedImage);
  }

  // _deletePicture() {
  //   setState(() {
  //     _storedImage = null;
  //     widget.onSelectImage(null);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 168,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 5,
                blurRadius: 15,
              ),
            ],
            borderRadius: BorderRadius.circular(15),
          ),
          child: InkWell(
            onTap: _takePicture,
            child: _storedImage != null
                ? ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    child: Image.file(_storedImage, fit: BoxFit.cover))
                : Center(
                    child: Image.asset(
                      'assets/images/upload_photo.png',
                      width: 70,
                      height: 70,
                    ),
                  ),
          ),
        ),
        SizedBox(height: 12),
//        _storedImage != null
//            ? FlatButton(
//                child: Text(
//                  'Delete issue photo',
//                  style: TextStyle(color: colorDarkRed),
//                ),
//                onPressed: _deletePicture,
//              )
//            :
        Text(
          'Upload issue photo',
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}

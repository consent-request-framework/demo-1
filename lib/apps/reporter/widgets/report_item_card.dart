import 'dart:io';

import 'package:demo1/apps/reporter/consts/status_const.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/common/helpers/consent_message.dart';
import 'package:demo1/enums/report_status_enum.dart';
import 'package:demo1/models/report.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReportItemCard extends StatelessWidget {
  final ReportItemCardModel reportItem;
  final Function onTap;
  final bool enabled;

  const ReportItemCard({
    @required this.reportItem,
    this.enabled = true,
    this.onTap,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;
    return AbsorbPointer(
      absorbing: !enabled,
      child: Card(
        color: enabled ? Colors.white : Colors.black12.withOpacity(0.05),
        elevation: enabled ? 3 : 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
          side: enabled
              ? BorderSide.none
              : BorderSide(color: Colors.black26.withOpacity(0.07)),
        ),
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: reportItem.image != null
                      ? Image(
                          fit: BoxFit.cover,
                          image: FileImage(
                            reportItem.image,
                          ),
                          width: 95.0,
                          height: 95.0,
                        )
                      : null,
                ),
                SizedBox(width: 15),
                Flexible(
                  child: Container(
                    height: 95.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          reportItem.name,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Spacer(),
                        Row(
                          children: <Widget>[
                            ImageIcon(
                              AssetImage(
                                'assets/images/ic_location.png',
                              ),
                              size: 25,
                              color: colorDarkRed,
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Flexible(
                              child: Text(reportItem.address ?? 'address'),
                            ),
                          ],
                        ),
                        Spacer(),
                        RichText(
                          text: TextSpan(
                            text: 'Status: ',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                            children: <TextSpan>[
                              profile.isReportStatusAvailable()
                                  ? TextSpan(
                                      text: Report.getStatusString(
                                          reportItem.status),
                                      style: TextStyle(
                                        color: StatusConst
                                            .statusColors[reportItem.status],
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  : ConsentMessage.reportNoStatusTextSpan(),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ReportItemCardModel {
  final String name;
  final File image;
  final String address;
  final ReportStatusEnum status;

  ReportItemCardModel({
    this.name,
    this.image,
    this.address,
    this.status,
  });
}

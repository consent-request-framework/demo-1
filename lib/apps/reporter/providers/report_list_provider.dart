import 'package:demo1/common/constants.dart';
import 'package:demo1/database/database.dart';
import 'package:demo1/enums/report_status_enum.dart';
import 'package:demo1/fake_backend/fake_report_backend.dart';
import 'package:demo1/models/report.dart';
import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';

class ReportListProvider with ChangeNotifier {
  final UsagePurposeRepository usageRepo;
  List<Report> _items = [];

  ReportListProvider(this.usageRepo);

  List<Report> get items {
    return [..._items];
  }

  int _totalReports = 0;

  int getTotalReports() {
    usageRepo.statisticIsViewed();
    return _totalReports;
  }

  int _totalScores = 0;

  int get totalScores => _totalScores;

  Future<void> initStatistic() async {
    final reportDb = await DB.query(Report.table);
    List<Report> reports =
        reportDb.map<Report>((x) => Report.fromMap(x)).toList();

    _totalReports = reports.length;
    _totalScores = getSum(reports.map((e) => e.score).toList());

    notifyListeners();
  }

  int getSum(List<int> list) {
    print('ReportListProvider.getSum');
    if (list == null || list.length == 0) {
      return 0;
    }
    return list.reduce((a, b) => a + b);
  }

  Future<void> addReport(Report report) async {
    String _address =
        await getAddress(report.location.latitude, report.location.longitude);
    Report item = report.copyWith(
      score: 0,
      address: _address,
      timestamp: DateTime.now(),
      statusMessage: 'Your report has been submitted.',
      status: ReportStatusEnum.submitted,
    );
    item.id = await DB.insert(Report.table, item);
    _items.insert(0, item);

    await usageRepo.reportIsAdded();
    await initStatistic();

    notifyListeners();
  }

  static Future<String> getAddress(double latitude, double longitude) async {
    final addresses = await Geocoder.google(apiKey, language: 'en')
        .findAddressesFromCoordinates(Coordinates(latitude, longitude));
    final firstAddress = addresses.first.addressLine;
    return firstAddress;
  }

  Future<void> fetchReports(context) async {
    await FakeReportBackend.updateStatuses(context);
    await initStatistic();

    final dataList = await DB.query(Report.table, orderBy: 'id DESC');
    _items = dataList.map<Report>((item) => Report.fromMap(item)).toList();
    notifyListeners();
  }

  Report findById(int id) {
    return _items.firstWhere((report) => report.id == id);
  }

  Map<DateTime, int> getReportStat() {
    Map<DateTime, int> result = {};
    for (var item in _items) {
      var timestamp = item.timestamp;
      var date = DateTime(timestamp.year, timestamp.month, timestamp.day);
      if (!result.containsKey(date)) {
        result[date] = 0;
      }

      result[date] += 1;
    }
    return result;
  }

  Map<DateTime, int> getScoreStat() {
    print('ReportListProvider.getScoreStat');
    Map<DateTime, int> result = {};
    for (var item in _items) {
      var timestamp = item.timestamp;
      var date = DateTime(timestamp.year, timestamp.month, timestamp.day);
      if (!result.containsKey(date)) {
        result[date] = 0;
      }

      result[date] += item.score;
    }
    return result;
  }
}

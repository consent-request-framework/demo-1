import 'package:demo1/common/constants.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppBarButton extends StatelessWidget {
  final int appIndex;
  final String iconPath;
  final String label;

  const AppBarButton({
    Key key,
    @required this.appIndex,
    @required this.iconPath,
    @required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<AppProvider>(context);
    return InkWell(
      borderRadius: BorderRadius.all(
        Radius.circular(15),
      ),
      onTap: () {
        provider.currentIndex = appIndex;
      },
      child: Container(
        width: 80,
        height: 80,
        child: Material(
          elevation: 3,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: provider.currentIndex == appIndex
                  ? colorDarkRed
                  : Colors.transparent,
            ),
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(height: 5.0),
              Center(
                child: Image.asset(
                  iconPath,
                  width: 50,
                  height: 50,
                  fit: BoxFit.scaleDown,
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                label,
                style: TextStyle(
                  color: provider.currentIndex == appIndex
                      ? colorDarkRed
                      : Colors.black87.withOpacity(0.7),
                  fontFamily: 'France',
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

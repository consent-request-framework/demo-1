import 'package:flutter/material.dart';

import 'app_bar_button.dart';

class AppButtonBar extends StatelessWidget {
  const AppButtonBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.white,
      shape: CircularNotchedRectangle(),
      child: Container(
        height: 100,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AppBarButton(
                label: 'Map',
                appIndex: 0,
                iconPath: 'assets/images/icons/app_bar_map.png',
              ),
              AppBarButton(
                label: 'Reporter',
                appIndex: 1,
                iconPath: 'assets/images/icons/app_bar_reporter.png',
              ),
              AppBarButton(
                label: 'Shop',
                appIndex: 2,
                iconPath: 'assets/images/icons/app_bar_shop.png',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

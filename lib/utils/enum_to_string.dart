import 'package:demo1/enums/filter_group_enum.dart';

class FilterUtil {

  static String getTitle(FilterEnum groupEnum) {
    switch (groupEnum) {
      case FilterEnum.bike:
        return 'Bikes';
      case FilterEnum.poi:
        return 'POI';
      case FilterEnum.restaurant:
        return 'Restaurants';
      case FilterEnum.park:
        return 'Parks';
    }

    return '';
  }

  static String getIconPath(FilterEnum groupEnum, bool isSelected) {
    switch (groupEnum) {
      case FilterEnum.bike:
        return isSelected
            ? 'assets/images/ic_bikes_selected.png'
            : 'assets/images/ic_bikes.png';
      case FilterEnum.poi:
        return isSelected
            ? 'assets/images/ic_poi_selected.png'
            : 'assets/images/ic_poi.png';
      case FilterEnum.restaurant:
        return isSelected
            ? 'assets/images/ic_restaurants_selected.png'
            : 'assets/images/ic_restaurants.png';
      case FilterEnum.park:
        return isSelected
            ? 'assets/images/ic_parks_selected.png'
            : 'assets/images/ic_parks.png';
    }

    return '';
  }
}

enum ReportStatusEnum {
  submitted,
  inProgress,
  rejected,
  done,
}

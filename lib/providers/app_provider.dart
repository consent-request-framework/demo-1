import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/foundation.dart';

class AppProvider with ChangeNotifier {
  final UsagePurposeRepository usageRepo;
  int _antiLoopIndex = -1;
  int _previousIndex = 0;
  int _currentIndex = 4;
  bool _showNavBar = true;

  AppProvider(this.usageRepo);

  get navBarAvailable => _currentIndex < 3 && _showNavBar;

  get currentIndex => _currentIndex;

  set defaultIndex(int index) {
    _currentIndex = index;
  }

  set currentIndex(int index) {
    if (_currentIndex == index) {
      return;
    }
    _previousIndex = _currentIndex;
    _currentIndex = index;
    notifyListeners();
  }

  void hideNavBar() {
    _showNavBar = false;
    notifyListeners();
  }

  void showNavBar() {
    _showNavBar = true;
    notifyListeners();
  }

  void goBack() {
    if ((currentIndex == 3 || currentIndex == 5) && _previousIndex == 4) {
      _previousIndex = _antiLoopIndex;
    }
    currentIndex = _previousIndex;
  }

  void goToInfoMap() {
    currentIndex = 0;
  }

  void goToReporter() {
    currentIndex = 1;
  }

  void goToShop() {
    currentIndex = 2;
  }

  void goToProfile() {
    currentIndex = 3;
  }

  goToConsent() {
    if ((currentIndex == 3 || currentIndex == 5) && _previousIndex != 4) {
      _antiLoopIndex = _previousIndex;
    }
    currentIndex = 4;
  }

  goToPreferences() {
    currentIndex = 5;
  }
}

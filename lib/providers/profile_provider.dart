import 'dart:async';

import 'package:collection/collection.dart';
import 'package:demo1/apps/consent/helpers/confirm_consent_helper.dart';
import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/database/database.dart';
import 'package:demo1/models/profile.dart';
import 'package:demo1/models/report.dart';
import 'package:demo1/models/review.dart';
import 'package:demo1/models/shop_order.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class ProfileProvider with ChangeNotifier {
  final UsagePurposeRepository usageRepo;
  Timer usageTimer;
  Profile _profile = Profile();
  BuildContext homeContext;

  ProfileProvider(this.usageRepo);

  void initTimer() {
    print('_initTimer');
    if (usageTimer != null) {
      usageTimer.cancel();
    }
    usageTimer = Timer.periodic(Duration(seconds: 2), (timer) {
      print('isConfirmed: ${_profile.isConsentConfirmed}');
      if (_profile.selectedTemplateId == 8 &&
          usageRepo.isReady() &&
          !_profile.isConsentConfirmed) {
        usageTimer.cancel();
        ConfirmConsentHelper.showConfirmMessage(homeContext);
      }
    });
  }

  Profile get profile {
    return _profile.copyWith();
  }

  bool get hasConsent {
    if (_profile.selectedTemplateId == null) {
      return false;
    }

    if (_profile.selectedTemplateId == 8 && !usageRepo.isReady()) {
      return true;
    }

    if (_profile.selectedTemplateId == 8 &&
        usageRepo.isReady() &&
        !_profile.isConsentConfirmed) {
      return false;
    }

    return true;
  }

  Future<void> saveProfile(Profile profile) async {
    Profile item = profile.copyWith(
      id: 1,
      timestamp: DateTime.now(),
    );
    item.id = await DB.update(Profile.table, item);

    if (_profile.avatar == null && item.avatar != null) {
      await usageRepo.avatarIsAdded();
    }

    Function eq = const ListEquality().equals;
    if (_profile.selectedTemplateId != item.selectedTemplateId ||
        eq(_profile.selectedPurposeIds, item.selectedPurposeIds)) {
      final url = 'https://cure.wu.ac.at/consents';
      Map<String, String> headers = {"Content-type": "application/json"};
      String json = '{'
          '"userId": "${profile.uuid}", '
          '"templateId": "${profile.selectedTemplateId}", '
          '"purposeIds": "${profile.selectedPurposeIds}"'
          '}';
      await http.post(url, headers: headers, body: json);
    }

    _profile = item.copyWith();

    notifyListeners();
  }

  Future<void> fetchAll(BuildContext ctx) async {
    print('fetchAll');
    homeContext = ctx;
    final profile =
        await DB.query(Profile.table, where: 'id = ?', whereArgs: [1]);
    var result = profile.map<Profile>((x) => Profile.fromMap(x)).toList().first;
    if (result.uuid == null) {
      result = result.copyWith(uuid: Uuid().v4());
      await DB.update(Profile.table, result);
    }

    final app = Provider.of<AppProvider>(ctx, listen: false);
    app.defaultIndex = (result.selectedTemplateId == null) ? 4 : 0;

    _profile = result;

    if (result.isMapUsePredefinedLocationGroups()) {
      Provider.of<ReviewListProvider>(ctx, listen: false)
          .initPreselection(_profile.selectedMapFilters);
    }

    await Provider.of<ReviewListProvider>(ctx, listen: false).initStatistic();

    if (_profile.selectedTemplateId == 8 && !_profile.isConsentConfirmed) {
      initTimer();
    }

    notifyListeners();
  }

  Future<void> updateTotalScore() async {
    final orderDb = await DB.query(ShopOrder.table);
    List<int> orders = orderDb
        .map<ShopOrder>((x) => ShopOrder.fromMap(x))
        .map((x) => x.shopItemPrice)
        .toList();

    final reportDb = await DB.query(Report.table);
    List<int> reports = reportDb
        .map<Report>((x) => Report.fromMap(x))
        .map((x) => x.score)
        .toList();

    final reviewDb = await DB.query(UserReview.table);
    List<UserReview> reviews =
        reviewDb.map<UserReview>((x) => UserReview.fromMap(x)).toList();

    final profile = await DB.query(
      Profile.table,
      where: 'id = ?',
      whereArgs: [1],
    );
    var result = profile.map<Profile>((x) => Profile.fromMap(x)).toList().first;
    int usedScores = getSum(orders);
    int reportScores = getSum(reports);
    int reviewScores = reviews.length * 12;
    result = result.copyWith(age: reportScores + reviewScores - usedScores);
    await DB.update(Profile.table, result);

    _profile = result;

    notifyListeners();
  }

  int getSum(List<int> list) {
    if (list == null || list.length == 0) {
      return 0;
    }
    return list.reduce((a, b) => a + b);
  }
}

import 'dart:math';

import 'package:demo1/database/database.dart';
import 'package:demo1/enums/report_status_enum.dart';
import 'package:demo1/models/report.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:provider/provider.dart';

class FakeReportBackend {
  static Future<void> updateStatuses(context) async {
    List<Report> submitted = await getByStatus(ReportStatusEnum.submitted);
    List<Report> inProgress = await getByStatus(ReportStatusEnum.inProgress);
    await updateSubmitted(submitted);
    await updateInProgress(inProgress, context);
  }

  static Future updateSubmitted(List<Report> items) async {
    if (items.length == 0) {
      return;
    }
    var item = items.first;
    if (item.timestamp.add(Duration(minutes: 1)).isAfter(DateTime.now())) {
      return;
    }
    item = item.copyWith(
      status: ReportStatusEnum.inProgress,
      statusMessage: 'Thank you for your reported issue. We are working on it.',
    );
    await DB.update(Report.table, item);
  }

  static Future updateInProgress(List<Report> items, context) async {
    if (items.length == 0) {
      return;
    }
    var item = items.first;
    if (item.timestamp.add(Duration(minutes: 3)).isAfter(DateTime.now())) {
      return;
    }
    if (Random().nextBool()) {
      item = item.copyWith(
        status: ReportStatusEnum.done,
        score: 21,
        statusMessage:
            'We have fixed the issue. Thank you for improving your city.',
      );
    } else {
      item = item.copyWith(
        status: ReportStatusEnum.rejected,
        score: 8,
        statusMessage:
            'Thank you for reporting the issue, but it was fixed earlier.',
      );
    }
    await DB.update(Report.table, item);
    await Provider.of<ProfileProvider>(context, listen: false)
        .updateTotalScore();
  }

  static Future<List<Report>> getByStatus(ReportStatusEnum status) async {
    final dataList = await DB.query(
      Report.table,
      where: 'status = ?',
      whereArgs: [status.toString()],
    );

    return dataList.map<Report>((item) => Report.fromMap(item)).toList();
  }
}

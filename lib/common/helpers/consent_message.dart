import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConsentMessage {
  static profileNoAvatar(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t given consent to process your profile photo',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static profileNoFullName(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t given consent to process your first and last name',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static profileNoAddress(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t given consent to process your home address',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }
  static profileNoConsent(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t given consent to process data for your profile',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static reportNoConsentForList(BuildContext ctx) {
    final profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            !profile.isReportCreationAvailable()
                ? 'Sorry, you haven\'t given consent to process your data to report an issue'
                : 'Sorry, you haven\'t given consent to process your data  to see a list of issues',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static mapUsePredefinedLocationGroups(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t given consent to process your data to use preferences',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static preferencesNoConsent(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t given consent to process your data to use preferences',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static reportNoConsentForStatistic(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t provided consent to process your data to view statistics',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static reportNoStatusTextSpan() {
    return TextSpan(
      text: 'Unavailable, because no consent is given',
      style: TextStyle(color: Colors.red),
    );
  }

  static reportNoStatusText(ctx) {
    return Column(
      children: <Widget>[
        Text(
          'Status unavailable, because no consent is given',
          textAlign: TextAlign.center,
        ),
        getConsentButton(ctx)
      ],
    );
  }

  static getConsentButton(ctx) {
    return RaisedButton(
      onPressed: () {
        var appProvider = Provider.of<AppProvider>(ctx, listen: false);
        appProvider.goToConsent();
      },
      shape: StadiumBorder(
        side: BorderSide(color: Colors.transparent),
      ),
      child: Text(
        'Adjust Consent',
        style: TextStyle(
          fontFamily: 'Montserrat',
          fontSize: 18,
          color: Colors.black87,
        ),
      ),
    );
  }

  static mapNoConsentForStatistic(BuildContext ctx) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t provided consent to process your data to view statistics',
            textAlign: TextAlign.center,
          ),
          getConsentButton(ctx)
        ],
      ),
    );
  }

  static mapNoLocationGroup() {
    return Text(
      'Unavailable, because no consent is given',
      style: TextStyle(color: Colors.red),
    );
  }

  static mapNoLeaveReview() {
    return Container(
      child: Flexible(
//        fit: BoxFit.fill,
        child: Text(
          'You cannot leave a review, because no consent is given',
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  static mapNoReview() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        'Your review is unavailable, because no consent is given',
        style: TextStyle(color: Colors.red),
      ),
    );
  }

  static noPurchase(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Sorry, you haven\'t provided consent to process your data to exchange points for tickets',
            textAlign: TextAlign.center,
          ),
          getConsentButton(context)
        ],
      ),
    );
  }
}

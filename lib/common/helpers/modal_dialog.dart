import 'package:flutter/material.dart';

import '../constants.dart';

class ModalDialog {
  static Future<void> showWarning(
    BuildContext context, {
    @required Widget body,
  }) async {
    return await showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
          content: Container(
            height: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                body,
                SizedBox(height: 25),
                FlatButton(
                  child: Text(
                    'OK',
                    style: mainButtonTextStyle,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  static Future<bool> showAreYouSure(
    BuildContext context, {
    @required Widget body,
    String yesBtn = 'Yes',
    String noBtn = 'No',
  }) async {
    return await showDialog<bool>(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(15.0),
                ),
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    body
                  ]
                ),
              ),
              actions: <Widget>[
                RaisedButton(
                  child: Text(
                    noBtn,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 18,
                      color: Colors.black87,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  shape: StadiumBorder(
                    side: BorderSide(color: Colors.transparent),
                  ),
                ),
                RaisedButton(
                  child: Text(
                    yesBtn,
                    style: mainButtonTextStyle,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            );
          },
        ) ??
        false;
  }
}

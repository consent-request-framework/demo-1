import 'package:flutter/material.dart';

import 'constants.dart';

class Badge extends StatelessWidget {
  final Widget child;
  final String value;
  final Color color;

  const Badge({
    Key key,
    @required this.child,
    @required this.value,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      children: [
        child,
        Positioned(
          right: 2,
          top: 2,
          child: Container(
            padding: EdgeInsets.fromLTRB(6.0, 2.0, 6.0, 2.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: colorDarkRed,
            ),
            constraints: BoxConstraints(
              minWidth: 32,
              minHeight: 14,
            ),
            child: Text(
              value,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Positioned(
          right: 5,
          top: 21,
          child: Image.asset(
            'assets/images/icons/avatar_arrow.png',
            width: 10,
            height: 10,
            fit: BoxFit.scaleDown,
            color: Colors.black87,
          ),
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';

const String apiKey = 'no_api_key';
const colorDarkRed = Color(0xFFE22028);
const colorGold = Color(0xFFFE9644);
const colorGreen = Color(0xFF10AE0C);

const mainButtonTextStyle = TextStyle(color: colorDarkRed, fontSize: 18);

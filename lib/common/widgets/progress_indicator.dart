import 'package:demo1/common/constants.dart';
import 'package:flutter/material.dart';

class ProgressIndicator extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 90,
        height: 90,
        child: CircularProgressIndicator(
          strokeWidth: 10,
          valueColor: AlwaysStoppedAnimation<Color>(colorDarkRed),
        ),
      ),
    );
  }
}

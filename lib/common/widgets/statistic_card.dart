// import 'package:flutter/material.dart';
//
// import 'bar_chart.dart';
//
// class UsageStatisticChart extends StatefulWidget {
//   final String title;
//   final List<TimeSeriesSales> chartData = [];
//
//   UsageStatisticChart(this.title, Map<DateTime, int> data) {
//     data.forEach((key, value) => chartData.add(TimeSeriesSales(key, value)));
//   }
//
//   @override
//   _UsageStatisticChartState createState() => _UsageStatisticChartState();
// }
//
// class _UsageStatisticChartState extends State<UsageStatisticChart> {
//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       margin: EdgeInsets.all(8.0),
//       child: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Text(widget.title),
//             LayoutBuilder(
//               builder: (BuildContext ctx, BoxConstraints boxConstraints) {
//                 return SingleChildScrollView(
//                   child: ConstrainedBox(
//                     constraints: BoxConstraints(
//                       maxWidth: boxConstraints.maxWidth,
//                     ),
//                     child: Container(
//                       height: 220,
//                       child: BarChart(widget.chartData),
//                     ),
//                   ),
//                 );
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

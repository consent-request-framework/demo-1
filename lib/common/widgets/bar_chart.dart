// import 'package:charts_flutter/flutter.dart' as charts;
// import 'package:flutter/material.dart';
//
// class BarChart extends StatelessWidget {
//   List<charts.Series<TimeSeriesSales, DateTime>> seriesList;
//   DateTime now = DateTime.now();
//
//   BarChart(List<TimeSeriesSales> data) {
//     seriesList = _createSampleData(data);
//   }
//
//   static List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData(
//     List<TimeSeriesSales> data,
//   ) {
//     return [
//       new charts.Series<TimeSeriesSales, DateTime>(
//           id: 'stat',
//           domainFn: (TimeSeriesSales sales, _) => sales.time,
//           measureFn: (TimeSeriesSales sales, _) => sales.sales,
//           data: data,
//           labelAccessorFn: (TimeSeriesSales sales, _) => '${sales.sales}')
//     ];
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return charts.TimeSeriesChart(
//       seriesList,
//       animate: false,
//       defaultRenderer: charts.BarRendererConfig<DateTime>(
//         barRendererDecorator: new charts.BarLabelDecorator<DateTime>(),
//       ),
//       defaultInteractions: true,
//       behaviors: [
//         new charts.SlidingViewport(),
//         new charts.PanAndZoomBehavior(),
//       ],
//       domainAxis: charts.DateTimeAxisSpec(
//         viewport: charts.DateTimeExtents(
//           start: now.subtract(Duration(days: 3)),
//           end: now.add(Duration(days: 1)),
//         ),
//       ),
//     );
//   }
// }
//
// class TimeSeriesSales {
//   final DateTime time;
//   final int sales;
//
//   TimeSeriesSales(this.time, this.sales);
// }

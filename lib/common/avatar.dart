import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'badge.dart';

class Avatar extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    var profile = Provider.of<ProfileProvider>(ctx, listen: false).profile;

    Future<void> _launchURL(String url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    Future<void> _gotoScreen(int value) async {
      var appProvider = Provider.of<AppProvider>(ctx, listen: false);
      if (value == 0) {
        appProvider.goToProfile();
      } else if (value == 1) {
        appProvider.goToPreferences();
      } else if (value == 2) {
        appProvider.goToConsent();
      } else if (value == 3) {
        String uuid = profile.uuid;
        int templateId = profile.selectedTemplateId;
        String purposeIds = profile.selectedPurposeIds != null
            ? profile.selectedPurposeIds.join('-')
            : 'null';
        final survey = 'https://survey.wu.ac.at/cure/index.php/441759?';
        final params =
            'userid=$uuid&templateid=$templateId--${profile.templates}&purposeids=$purposeIds&newtest=Y';
        await _launchURL('$survey$params');
      }
    }

    return InkWell(
      onTap: () => {
        showMenu(
          elevation: 3,
          context: ctx,
          position: new RelativeRect.fromLTRB(1.0, 50.0, 0.0, 0.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          items: <PopupMenuEntry>[
            PopupMenuItem(
              value: 0,
              child: Container(
                alignment: Alignment.center,
                child: Text('Profile'),
              ),
            ),
            PopupMenuDivider(),
            PopupMenuItem(
              value: 1,
              child: Container(
                alignment: Alignment.center,
                child: Text('Preferences'),
              ),
            ),
            PopupMenuDivider(),
            PopupMenuItem(
              value: 2,
              child: Container(
                alignment: Alignment.center,
                child: Text('Consent'),
              ),
            ),
            PopupMenuDivider(),
            PopupMenuItem(
              value: 3,
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: <Widget>[
                    Text('Go to Survey'),
                    Text(
                      '(${profile.getShortId()})',
                      style: TextStyle(fontSize: 10),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ).then((value) => {_gotoScreen(value)}),
      },
      child: Container(
        width: 65,
        padding: const EdgeInsets.all(2.0),
        child: Consumer<ProfileProvider>(
          builder: (ctx, data, ch) => Badge(
            value: data.profile.age != null ? data.profile.age.toString() : '0',
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(const Radius.circular(25.0)),
                border:
                    Border.all(width: 1, color: Colors.grey.withOpacity(0.8)),
              ),
              child: data.profile.avatar != null
                  ? CircleAvatar(
                      radius: 18,
                      backgroundImage: FileImage(data.profile.avatar),
                    )
                  : null,
            ),
          ),
        ),
      ),
    );
  }
}

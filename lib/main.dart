import 'package:demo1/apps/shop/providers/shop_item_list_provider.dart';
import 'package:demo1/common/screens/not_found_screen.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:demo1/repositories/usage_purpose_repository.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'appbar/widgets/app_bar.dart';
import 'apps/consent/consent_app.dart';
import 'apps/consent/providers/purpose_list_provider.dart';
import 'apps/consent/providers/template_list_provider.dart';
import 'apps/infomap/infomap_app.dart';
import 'apps/infomap/providers/review_list_provider.dart';
import 'apps/preferences/preferences_app.dart';
import 'apps/profile/profile_app.dart';
import 'apps/reporter/providers/report_list_provider.dart';
import 'apps/reporter/reporter_app.dart';
import 'apps/shop/providers/shop_order_list_provider.dart';
import 'apps/shop/shop_app.dart';
import 'common/constants.dart';
import 'database/database.dart';
import 'providers/app_provider.dart';

class HomePage extends StatelessWidget {
  final PageStorageBucket bucket = PageStorageBucket();


  Widget _getPage(int index) {
    switch (index) {
      case 0:
        return InfomapApp(key: PageStorageKey('InfomapApp'));
      case 1:
        return ReporterApp(key: PageStorageKey('ReporterApp'));
      case 2:
        return ShopApp(key: PageStorageKey('ShopApp'));
      case 3:
        return ProfileApp(key: PageStorageKey('ProfileApp'));
      case 4:
        return ConsentApp(key: PageStorageKey('ConsentApp'));
      case 5:
        return PreferencesApp(key: PageStorageKey('PreferencesApp'));
    }

    return NotFoundScreen('global');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<ProfileProvider>(context, listen: false)
          .fetchAll(context),
      builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
          ? Center(child: CircularProgressIndicator())
          : Consumer<AppProvider>(
              builder: (ctx, app, ch) => Scaffold(
                body: SafeArea(
                  child: PageStorage(
                    child: _getPage(app.currentIndex),
                    bucket: bucket,
                  ),
                ),
                bottomNavigationBar:
                    app.navBarAvailable ? AppButtonBar() : null,
              ),
            ),
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DB.init();

  final usageRepo = UsagePurposeRepository();
  await usageRepo.init();
  final themeData = ThemeData(fontFamily: 'Montserrat');
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: themeData.copyWith(
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black54,
          ),
          textTheme: TextTheme(
            headline6: TextStyle(
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 16,
            ),
          ),
        ),
        scaffoldBackgroundColor: Colors.white,
        buttonTheme: ButtonThemeData(
          height: 41,
          shape: StadiumBorder(side: BorderSide(color: colorDarkRed)),
          buttonColor: Colors.white,
          highlightColor: colorDarkRed.withOpacity(0.15),
          splashColor: colorDarkRed.withOpacity(0.5),
        ),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(color: Colors.black45),
          labelStyle: TextStyle(color: Colors.black45),
          contentPadding: EdgeInsets.only(left: 12, top: 12),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
          ),
        ),
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: AppProvider(usageRepo)),
          ChangeNotifierProvider.value(value: ReviewListProvider(usageRepo)),
          ChangeNotifierProvider.value(value: ReportListProvider(usageRepo)),
          ChangeNotifierProvider.value(value: ShopItemListProvider()),
          ChangeNotifierProvider.value(value: ShopOrderListProvider(usageRepo)),
          ChangeNotifierProvider.value(value: ProfileProvider(usageRepo)),
          ChangeNotifierProvider.value(value: TemplateListProvider()),
          ChangeNotifierProvider.value(value: PurposeListProvider(usageRepo)),
        ],
        child: HomePage(),
      ),
    ),
  );
}

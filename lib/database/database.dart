import 'dart:async';

import 'package:demo1/models/model.dart';
import 'package:sqflite/sqflite.dart';

import 'migrations.dart';

abstract class DB {
  static Database _db;

  static int get _version => 15;

  static Future<void> init() async {
    if (_db != null) {
      return;
    }

    try {
      String _path = await getDatabasesPath() + '/demo1';
      _db = await openDatabase(_path,
          version: _version, onCreate: onCreate, onUpgrade: onUpgrade);
    } catch (ex) {
      print(ex);
    }
  }

  static void onCreate(Database db, int version) async {
    await db.execute(Migrations.SCRIPT_1);
    await db.execute(Migrations.SCRIPT_2);
    await db.execute(Migrations.SCRIPT_3);
    await db.execute(Migrations.SCRIPT_4);
    await db.execute(Migrations.SCRIPT_REVIEW);
    await db.execute(Migrations.SCRIPT_PROFILE);
    await db.execute(Migrations.SCRIPT_PROFILE_INIT);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATE_1);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATE_2);
    await db.execute(Migrations.SCRIPT_SHOP_ORDER_2);
    await db.execute(Migrations.SCRIPT_SHOP_ORDER_3);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATE_3);
    await db.execute(Migrations.SCRIPT_REPORT_ADD_ADDRESS);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_UUID);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_MAP_SELECTION);
    await db.execute(Migrations.SCRIPT_CREATE_USAGE_PURPOSE);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_IS_CONSENT_CONFIRMED);
    await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATES);
  }

  static void onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < 2) {
      await db.execute(Migrations.SCRIPT_2);
    }
    if (oldVersion < 3) {
      await db.execute(Migrations.SCRIPT_3);
    }
    if (oldVersion < 4) {
      await db.execute(Migrations.SCRIPT_4);
    }
    if (oldVersion < 5) {
      await db.execute(Migrations.SCRIPT_REVIEW);
    }
    if (oldVersion < 6) {
      await db.execute(Migrations.SCRIPT_PROFILE);
      await db.execute(Migrations.SCRIPT_PROFILE_INIT);
    }
    if (oldVersion < 7) {
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATE_1);
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATE_2);
    }
    if (oldVersion < 8) {
      await db.execute(Migrations.SCRIPT_SHOP_ORDER_2);
      await db.execute(Migrations.SCRIPT_SHOP_ORDER_3);
    }
    if (oldVersion < 9) {
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATE_3);
    }
    if (oldVersion < 10) {
      await db.execute(Migrations.SCRIPT_REPORT_ADD_ADDRESS);
    }
    if (oldVersion < 11) {
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_UUID);
    }
    if (oldVersion < 12) {
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_MAP_SELECTION);
    }
    if (oldVersion < 13) {
      await db.execute(Migrations.SCRIPT_CREATE_USAGE_PURPOSE);
    }
    if (oldVersion < 14) {
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_IS_CONSENT_CONFIRMED);
    }
    if (oldVersion < 15) {
      await db.execute(Migrations.SCRIPT_PROFILE_ADD_TEMPLATES);
    }
  }

  static Future<List<Map<String, dynamic>>> query(
    String table, {
    String where,
    List<dynamic> whereArgs,
    String orderBy,
  }) async =>
      _db.query(
        table,
        where: where,
        whereArgs: whereArgs,
        orderBy: orderBy,
      );

  static Future<int> insert(String table, Model model) async =>
      await _db.insert(table, model.toMap());

  static Future<int> update(String table, Model model) async => await _db
      .update(table, model.toMap(), where: 'id = ?', whereArgs: [model.id]);

  static Future<int> delete(String table, Model model) async =>
      await _db.delete(table, where: 'id = ?', whereArgs: [model.id]);

  static Future<int> rawDelete(String sql, [List<dynamic> arguments]) async =>
      await _db.rawDelete(sql, arguments);
}

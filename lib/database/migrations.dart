class Migrations {
  static const String SCRIPT_1 = '''
    CREATE TABLE report (
      id INTEGER PRIMARY KEY NOT NULL, 
      score STRING, 
      firstName STRING,
      lastName STRING,
      description STRING,
      imageUri STRING,
      statusMessage STRING,
      status STRING,
      latitude STRING,
      longitude STRING,
      timestamp INTEGER
    )
  ''';

  static const String SCRIPT_2 = '''
    CREATE TABLE shop_item (
      id INTEGER PRIMARY KEY NOT NULL, 
      price INTEGER, 
      name STRING
    )
  ''';

  static const String SCRIPT_3 = '''
    INSERT INTO shop_item (name, price) 
    VALUES
      ('Kunsthistorisches Museum', 13),
      ('Kunsthalle Vienna', 15),
      ('Belvedere Gallery Austria', 10),
      ('House of Music', 14),
      ('Albertina Graphic Arts Collection', 25),
      ('Museum of Military History', 50),
      ('Museum of Natural History', 100);
  ''';

  static const String SCRIPT_4 = '''
    CREATE TABLE shop_order (
      id INTEGER PRIMARY KEY NOT NULL, 
      shopItemPrice INTEGER, 
      shopItemName STRING,
      firstName STRING,
      lastName STRING,
      street STRING,
      zipCode INTEGER,
      city STRING,
      status STRING,
      timestamp INTEGER
    )
  ''';

  static const String SCRIPT_REVIEW = '''
    CREATE TABLE review (
      id INTEGER PRIMARY KEY NOT NULL, 
      score INTEGER, 
      firstName STRING,
      lastName STRING,
      placeId STRING,
      imageUri STRING,
      reviewText STRING,
      timestamp INTEGER
    )
  ''';

  static const String SCRIPT_PROFILE = '''
    CREATE TABLE profile (
      id INTEGER PRIMARY KEY NOT NULL, 
      avatarUri STRING,
      firstName STRING,
      lastName STRING,
      age INTEGER,
      street STRING,
      zipCode INTEGER,
      city STRING,
      timestamp INTEGER
    )
  ''';

  static const String SCRIPT_PROFILE_INIT = '''
    INSERT INTO profile (id) VALUES (1);
  ''';

  static const String SCRIPT_PROFILE_ADD_TEMPLATE_1 = '''
    ALTER TABLE profile ADD COLUMN selectedTemplateId INTEGER;
  ''';

  static const String SCRIPT_PROFILE_ADD_TEMPLATE_2 = '''
    ALTER TABLE profile ADD COLUMN selectedPurposeIds STRING;
  ''';

  static const String SCRIPT_SHOP_ORDER_2 = '''
    ALTER TABLE shop_order ADD COLUMN shopItemAddress STRING;
  ''';

  static const String SCRIPT_SHOP_ORDER_3 = '''
    ALTER TABLE shop_order ADD COLUMN shopItemImageUri STRING;
  ''';

  static const String SCRIPT_PROFILE_ADD_TEMPLATE_3 = '''
    ALTER TABLE profile ADD COLUMN selectedPreferenceIds STRING;
  ''';

  static const String SCRIPT_REPORT_ADD_ADDRESS = '''
    ALTER TABLE report ADD COLUMN address STRING;
  ''';

  static const String SCRIPT_PROFILE_ADD_UUID = '''
    ALTER TABLE profile ADD COLUMN uuid STRING;
  ''';

  static const String SCRIPT_PROFILE_ADD_MAP_SELECTION = '''
    ALTER TABLE profile ADD COLUMN selectedMapFilters STRING;
  ''';

  static const String SCRIPT_CREATE_USAGE_PURPOSE = '''
    CREATE TABLE usage_purpose (
      id INTEGER PRIMARY KEY NOT NULL, 
      purposeId INTEGER,
      timestamp INTEGER
    )
  ''';

  static const String SCRIPT_PROFILE_ADD_IS_CONSENT_CONFIRMED = '''
    ALTER TABLE profile ADD COLUMN isConsentConfirmed INTEGER;
  ''';

  static const String SCRIPT_PROFILE_ADD_TEMPLATES = '''
    ALTER TABLE profile ADD COLUMN templates STRING;
  ''';

}

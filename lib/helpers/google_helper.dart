import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:demo1/apps/infomap/providers/review_list_provider.dart';
import 'package:demo1/common/constants.dart';
import 'package:demo1/providers/app_provider.dart';
import 'package:demo1/providers/profile_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_place/google_place.dart';
import 'package:location/location.dart' as GPS;
import 'package:provider/provider.dart';

class GoogleHelper {
  static Future<LatLng> getLocation(BuildContext ctx) async {
    final profilePrd = Provider.of<ProfileProvider>(ctx, listen: false);
    final defaultLocation = LatLng(48.2082, 16.3738);
    if (!profilePrd.profile.isMapShowCurrentLocation()) {
      return defaultLocation;
    }

    var location = new GPS.Location();
    var serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return defaultLocation;
      }
    }

    var permissionGranted = await location.hasPermission();
    if (permissionGranted == GPS.PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != GPS.PermissionStatus.granted) {
        return defaultLocation;
      }
    }

    final app = Provider.of<AppProvider>(ctx, listen: false);
    app.usageRepo.currentLocationIsShowed();

    final loc = await location.getLocation();

    return LatLng(loc.latitude, loc.longitude);
  }

  static Future<Set<Marker>> getMarkers(
      BuildContext ctx, LatLng location, Function onClick) async {
    Set<Marker> markers = {};
    final profilePrd = Provider.of<ProfileProvider>(ctx, listen: false);
    if (!profilePrd.profile.isMapShowLocationGroups()) {
      return markers;
    }

    final provider = Provider.of<ReviewListProvider>(ctx, listen: false);
    if (provider.parksSelected) {
      markers.addAll(await _buildMarkers(
        'park',
        'assets/images/ic_cafe_mark.png',
          location,
          onClick,
      ));
    }
    if (provider.restSelected) {
      markers.addAll(await _buildMarkers(
        'restaurant',
        'assets/images/ic_restaurant_mark.png',
        location,
        onClick,
      ));
    }
    if (provider.bikesSelected) {
      markers.addAll(await _buildMarkers(
        'bicycle_store',
        'assets/images/ic_bike_mark.png',
        location,
        onClick,
      ));
    }
    if (provider.poiSelected) {
      markers.addAll(await _buildMarkers(
        'point_of_interest',
        'assets/images/ic_poi_mark.png',
        location,
        onClick,
      ));
    }
    return markers;
  }

  static Future<Set<Marker>> _buildMarkers(
      String type, String iconPath, LatLng location, Function onClick) async {
    final Uint8List markerIcon = await _getBytesFromAsset(iconPath, 100);
    BitmapDescriptor icon = BitmapDescriptor.fromBytes(markerIcon);
    var placeResponse = await GooglePlace(apiKey).search.getNearBySearch(
        Location(lat: location.latitude, lng: location.longitude), 3000,
        type: type);
    if (placeResponse.results.isEmpty) {
      print("No results");
      return {};
    } else {
      var counter = 0;
      var markers = placeResponse.results
          .map((r) => Marker(
                markerId: MarkerId("markerId_${type}_${counter++}"),
                position:
                    LatLng(r.geometry.location.lat, r.geometry.location.lng),
                icon: icon,
                infoWindow: InfoWindow(
                    title: r.name,
                    snippet: r.vicinity,
                    onTap: () {
                      onClick(r);
                    }),
              ))
          .toSet();

      return markers;
    }
  }

  static Future<Uint8List> _getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }
}
